-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 05, 2021 at 10:02 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doantotnghiep_laravel_phukien`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'Product', 'Cập nhật product', 14, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":14,\"pro_name\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_slug\":\"bao-da-kiem-ban-phim-cho-samsung-galaxy-tab-s6\",\"pro_price\":\"230000\",\"pro_price_entry\":0,\"pro_category_id\":\"15\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__14.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_content\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:52:49\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 10:53:05\"},\"new\":{\"id\":14,\"pro_name\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_slug\":\"bao-da-kiem-ban-phim-cho-samsung-galaxy-tab-s6\",\"pro_price\":\"230000\",\"pro_price_entry\":0,\"pro_category_id\":\"15\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__14.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_content\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:52:49\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 10:53:05\"}}', '2020-08-17 03:53:05', '2020-08-17 03:53:05'),
(2, 'Product', 'Cập nhật product', 14, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":14,\"pro_name\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_slug\":\"bao-da-kiem-ban-phim-cho-samsung-galaxy-tab-s6\",\"pro_price\":\"230000\",\"pro_price_entry\":0,\"pro_category_id\":\"15\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__14.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_content\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:52:49\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 10:53:11\"},\"new\":{\"id\":14,\"pro_name\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_slug\":\"bao-da-kiem-ban-phim-cho-samsung-galaxy-tab-s6\",\"pro_price\":\"230000\",\"pro_price_entry\":0,\"pro_category_id\":\"15\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__14.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_content\":\"Bao da ki\\u00eam B\\u00e0n ph\\u00edm cho Samsung Galaxy Tab S6\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:52:49\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 10:53:11\"}}', '2020-08-17 03:53:11', '2020-08-17 03:53:11'),
(3, 'Product', 'Cập nhật product', 5, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":5,\"pro_name\":\"Chu\\u1ed9t Apple Magic Mouse 2\",\"pro_slug\":\"chuot-apple-magic-mouse-2\",\"pro_price\":\"2000000\",\"pro_price_entry\":0,\"pro_category_id\":\"20\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__5.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"5\",\"pro_import_goods\":0,\"pro_description\":\"Chu\\u1ed9t Apple Magic Mouse 2\",\"pro_content\":\"Chu\\u1ed9t Apple Magic Mouse 2\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:38:29\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":5,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:13:13\"},\"new\":{\"id\":5,\"pro_name\":\"Chu\\u1ed9t Apple Magic Mouse 2\",\"pro_slug\":\"chuot-apple-magic-mouse-2\",\"pro_price\":\"2000000\",\"pro_price_entry\":0,\"pro_category_id\":\"20\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__5.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"5\",\"pro_import_goods\":0,\"pro_description\":\"Chu\\u1ed9t Apple Magic Mouse 2\",\"pro_content\":\"Chu\\u1ed9t Apple Magic Mouse 2\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:38:29\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":5,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:13:13\"}}', '2020-08-17 04:13:13', '2020-08-17 04:13:13'),
(4, 'Product', 'Cập nhật product', 1, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":1,\"pro_name\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_slug\":\"tai-nghe-bluetooth-apple-airpods-2-vna\",\"pro_price\":\"3650000\",\"pro_price_entry\":0,\"pro_category_id\":\"16\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__1.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_content\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:34:19\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:13:59\"},\"new\":{\"id\":1,\"pro_name\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_slug\":\"tai-nghe-bluetooth-apple-airpods-2-vna\",\"pro_price\":\"3650000\",\"pro_price_entry\":0,\"pro_category_id\":\"16\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__1.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_content\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:34:19\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:13:59\"}}', '2020-08-17 04:13:59', '2020-08-17 04:13:59'),
(5, 'Product', 'Cập nhật product', 2, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":2,\"pro_name\":\"B\\u00fat c\\u1ea3m \\u1ee9ng Apple Pencil 2 MU8F2\",\"pro_slug\":\"but-cam-ung-apple-pencil-2-mu8f2\",\"pro_price\":\"3900000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__2.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"B\\u00fat c\\u1ea3m \\u1ee9ng Apple Pencil 2 MU8F2\",\"pro_content\":\"B\\u00fat c\\u1ea3m \\u1ee9ng Apple Pencil 2 MU8F2\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:35:16\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:14:54\"},\"new\":{\"id\":2,\"pro_name\":\"B\\u00fat c\\u1ea3m \\u1ee9ng Apple Pencil 2 MU8F2\",\"pro_slug\":\"but-cam-ung-apple-pencil-2-mu8f2\",\"pro_price\":\"3900000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__2.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"B\\u00fat c\\u1ea3m \\u1ee9ng Apple Pencil 2 MU8F2\",\"pro_content\":\"B\\u00fat c\\u1ea3m \\u1ee9ng Apple Pencil 2 MU8F2\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:35:16\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:14:54\"}}', '2020-08-17 04:14:54', '2020-08-17 04:14:54'),
(6, 'Product', 'Cập nhật product', 4, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":4,\"pro_name\":\"B\\u00e0n ph\\u00edm Smart Keyboard cho iPad Pro 10.5\\/Air 10.5\\/10.2\",\"pro_slug\":\"ban-phim-smart-keyboard-cho-ipad-pro-105air-105102\",\"pro_price\":\"3800000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__4.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"B\\u00e0n ph\\u00edm Smart Keyboard cho iPad Pro 10.5\\/Air 10.5\\/10.2\",\"pro_content\":\"B\\u00e0n ph\\u00edm Smart Keyboard cho iPad Pro 10.5\\/Air 10.5\\/10.2\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:37:39\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:06\"},\"new\":{\"id\":4,\"pro_name\":\"B\\u00e0n ph\\u00edm Smart Keyboard cho iPad Pro 10.5\\/Air 10.5\\/10.2\",\"pro_slug\":\"ban-phim-smart-keyboard-cho-ipad-pro-105air-105102\",\"pro_price\":\"3800000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__4.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"B\\u00e0n ph\\u00edm Smart Keyboard cho iPad Pro 10.5\\/Air 10.5\\/10.2\",\"pro_content\":\"B\\u00e0n ph\\u00edm Smart Keyboard cho iPad Pro 10.5\\/Air 10.5\\/10.2\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:37:39\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:06\"}}', '2020-08-17 04:15:06', '2020-08-17 04:15:06'),
(7, 'Product', 'Cập nhật product', 3, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":3,\"pro_name\":\"B\\u00fat Apple Pencil 1\",\"pro_slug\":\"but-apple-pencil-1\",\"pro_price\":\"3200000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__3.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"B\\u00fat Apple Pencil 1\",\"pro_content\":\"B\\u00fat Apple Pencil 1\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:36:27\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:19\"},\"new\":{\"id\":3,\"pro_name\":\"B\\u00fat Apple Pencil 1\",\"pro_slug\":\"but-apple-pencil-1\",\"pro_price\":\"3200000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__3.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"B\\u00fat Apple Pencil 1\",\"pro_content\":\"B\\u00fat Apple Pencil 1\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:36:27\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:19\"}}', '2020-08-17 04:15:19', '2020-08-17 04:15:19'),
(8, 'Product', 'Cập nhật product', 6, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":6,\"pro_name\":\"C\\u00e1p Lightning Apple 1m MQUE2 Ch\\u00ednh h\\u00e3ng\",\"pro_slug\":\"cap-lightning-apple-1m-mque2-chinh-hang\",\"pro_price\":\"490000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__6.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"C\\u00e1p Lightning Apple 1m MQUE2 Ch\\u00ednh h\\u00e3ng\",\"pro_content\":\"C\\u00e1p Lightning Apple 1m MQUE2 Ch\\u00ednh h\\u00e3ng\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:39:05\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:31\"},\"new\":{\"id\":6,\"pro_name\":\"C\\u00e1p Lightning Apple 1m MQUE2 Ch\\u00ednh h\\u00e3ng\",\"pro_slug\":\"cap-lightning-apple-1m-mque2-chinh-hang\",\"pro_price\":\"490000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__6.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"C\\u00e1p Lightning Apple 1m MQUE2 Ch\\u00ednh h\\u00e3ng\",\"pro_content\":\"C\\u00e1p Lightning Apple 1m MQUE2 Ch\\u00ednh h\\u00e3ng\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:39:05\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:31\"}}', '2020-08-17 04:15:31', '2020-08-17 04:15:31'),
(9, 'Product', 'Cập nhật product', 7, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":7,\"pro_name\":\"S\\u1ea1c Macbook Apple 61W USB-C Power Adapter MNF72 Ch\\u00ednh h\\u00e3ng\",\"pro_slug\":\"sac-macbook-apple-61w-usb-c-power-adapter-mnf72-chinh-hang\",\"pro_price\":\"2220000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"4\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__7.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"S\\u1ea1c Macbook Apple 61W USB-C Power Adapter MNF72 Ch\\u00ednh h\\u00e3ng\",\"pro_content\":\"S\\u1ea1c Macbook Apple 61W USB-C Power Adapter MNF72 Ch\\u00ednh h\\u00e3ng\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:39:41\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:41\"},\"new\":{\"id\":7,\"pro_name\":\"S\\u1ea1c Macbook Apple 61W USB-C Power Adapter MNF72 Ch\\u00ednh h\\u00e3ng\",\"pro_slug\":\"sac-macbook-apple-61w-usb-c-power-adapter-mnf72-chinh-hang\",\"pro_price\":\"2220000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"4\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__7.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"4\",\"pro_import_goods\":0,\"pro_description\":\"S\\u1ea1c Macbook Apple 61W USB-C Power Adapter MNF72 Ch\\u00ednh h\\u00e3ng\",\"pro_content\":\"S\\u1ea1c Macbook Apple 61W USB-C Power Adapter MNF72 Ch\\u00ednh h\\u00e3ng\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:39:41\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":4,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:41\"}}', '2020-08-17 04:15:41', '2020-08-17 04:15:41'),
(10, 'Product', 'Cập nhật product', 13, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":13,\"pro_name\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone 11 Pro - Mocoll Full \\u0110en Cao C\\u1ea5p\",\"pro_slug\":\"dan-cuong-luc-cho-iphone-11-pro-mocoll-full-den-cao-cap\",\"pro_price\":\"210000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"3\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__13.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone 11 Pro - Mocoll Full \\u0110en Cao C\\u1ea5p\",\"pro_content\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone 11 Pro - Mocoll Full \\u0110en Cao C\\u1ea5p\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:52:08\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:56\"},\"new\":{\"id\":13,\"pro_name\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone 11 Pro - Mocoll Full \\u0110en Cao C\\u1ea5p\",\"pro_slug\":\"dan-cuong-luc-cho-iphone-11-pro-mocoll-full-den-cao-cap\",\"pro_price\":\"210000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"3\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__13.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone 11 Pro - Mocoll Full \\u0110en Cao C\\u1ea5p\",\"pro_content\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone 11 Pro - Mocoll Full \\u0110en Cao C\\u1ea5p\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:52:08\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:15:56\"}}', '2020-08-17 04:15:56', '2020-08-17 04:15:56'),
(11, 'Product', 'Cập nhật product', 8, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":8,\"pro_name\":\"Mi\\u1ebfng d\\u00e1n Jcpal b\\u1ea3o v\\u1ec7 Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max\",\"pro_slug\":\"mieng-dan-jcpal-bao-ve-camera-sau-cho-iphone-11-11-pro-11-pro-max\",\"pro_price\":\"450000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":0,\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__8.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng d\\u00e1n Jcpal b\\u1ea3o v\\u1ec7 Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max\",\"pro_content\":\"Mi\\u1ebfng d\\u00e1n Jcpal b\\u1ea3o v\\u1ec7 Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:47:43\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:08\"},\"new\":{\"id\":8,\"pro_name\":\"Mi\\u1ebfng d\\u00e1n Jcpal b\\u1ea3o v\\u1ec7 Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max\",\"pro_slug\":\"mieng-dan-jcpal-bao-ve-camera-sau-cho-iphone-11-11-pro-11-pro-max\",\"pro_price\":\"450000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":0,\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__8.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng d\\u00e1n Jcpal b\\u1ea3o v\\u1ec7 Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max\",\"pro_content\":\"Mi\\u1ebfng d\\u00e1n Jcpal b\\u1ea3o v\\u1ec7 Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:47:43\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:08\"}}', '2020-08-17 04:16:08', '2020-08-17 04:16:08'),
(12, 'Product', 'Cập nhật product', 11, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":11,\"pro_name\":\"Mi\\u1ebfng d\\u00e1n PPF Full vi\\u1ec1n m\\u1eb7t sau cho iPhone X\\/XS\",\"pro_slug\":\"mieng-dan-ppf-full-vien-mat-sau-cho-iphone-xxs\",\"pro_price\":\"350000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__11.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng d\\u00e1n PPF Full vi\\u1ec1n m\\u1eb7t sau cho iPhone X\\/XS\",\"pro_content\":\"Mi\\u1ebfng d\\u00e1n PPF Full vi\\u1ec1n m\\u1eb7t sau cho iPhone X\\/XS\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:50:54\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:22\"},\"new\":{\"id\":11,\"pro_name\":\"Mi\\u1ebfng d\\u00e1n PPF Full vi\\u1ec1n m\\u1eb7t sau cho iPhone X\\/XS\",\"pro_slug\":\"mieng-dan-ppf-full-vien-mat-sau-cho-iphone-xxs\",\"pro_price\":\"350000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__11.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng d\\u00e1n PPF Full vi\\u1ec1n m\\u1eb7t sau cho iPhone X\\/XS\",\"pro_content\":\"Mi\\u1ebfng d\\u00e1n PPF Full vi\\u1ec1n m\\u1eb7t sau cho iPhone X\\/XS\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:50:54\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:22\"}}', '2020-08-17 04:16:22', '2020-08-17 04:16:22'),
(13, 'Product', 'Cập nhật product', 12, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":12,\"pro_name\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone XS Max - JCPal Full \\u0110en Cao C\\u1ea5p\",\"pro_slug\":\"dan-cuong-luc-cho-iphone-xs-max-jcpal-full-den-cao-cap\",\"pro_price\":\"150000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__12.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone XS Max - JCPal Full \\u0110en Cao C\\u1ea5p\",\"pro_content\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone XS Max - JCPal Full \\u0110en Cao C\\u1ea5p\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:51:31\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:34\"},\"new\":{\"id\":12,\"pro_name\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone XS Max - JCPal Full \\u0110en Cao C\\u1ea5p\",\"pro_slug\":\"dan-cuong-luc-cho-iphone-xs-max-jcpal-full-den-cao-cap\",\"pro_price\":\"150000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__12.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone XS Max - JCPal Full \\u0110en Cao C\\u1ea5p\",\"pro_content\":\"D\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c cho iPhone XS Max - JCPal Full \\u0110en Cao C\\u1ea5p\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:51:31\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:34\"}}', '2020-08-17 04:16:34', '2020-08-17 04:16:34'),
(14, 'Product', 'Cập nhật product', 10, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":10,\"pro_name\":\"Mi\\u1ebfng d\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c ch\\u1ed1ng nh\\u00ecn tr\\u1ed9m Mocoll Full 3D cho iPhone X \\/ Xs\",\"pro_slug\":\"mieng-dan-cuong-luc-chong-nhin-trom-mocoll-full-3d-cho-iphone-x-xs\",\"pro_price\":\"250000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__10.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"1\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng d\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c ch\\u1ed1ng nh\\u00ecn tr\\u1ed9m Mocoll Full 3D cho iPhone X \\/ Xs\",\"pro_content\":\"Mi\\u1ebfng d\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c ch\\u1ed1ng nh\\u00ecn tr\\u1ed9m Mocoll Full 3D cho iPhone X \\/ Xs\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:50:08\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":1,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:44\"},\"new\":{\"id\":10,\"pro_name\":\"Mi\\u1ebfng d\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c ch\\u1ed1ng nh\\u00ecn tr\\u1ed9m Mocoll Full 3D cho iPhone X \\/ Xs\",\"pro_slug\":\"mieng-dan-cuong-luc-chong-nhin-trom-mocoll-full-3d-cho-iphone-x-xs\",\"pro_price\":\"250000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"1\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__10.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"1\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng d\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c ch\\u1ed1ng nh\\u00ecn tr\\u1ed9m Mocoll Full 3D cho iPhone X \\/ Xs\",\"pro_content\":\"Mi\\u1ebfng d\\u00e1n c\\u01b0\\u1eddng l\\u1ef1c ch\\u1ed1ng nh\\u00ecn tr\\u1ed9m Mocoll Full 3D cho iPhone X \\/ Xs\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:50:08\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":1,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:44\"}}', '2020-08-17 04:16:44', '2020-08-17 04:16:44'),
(15, 'Product', 'Cập nhật product', 9, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":9,\"pro_name\":\"Mi\\u1ebfng D\\u00e1n PPF Full M\\u00e0n h\\u00ecnh Cao c\\u1ea5p cho Samsung Galaxy Note 10 Plus\",\"pro_slug\":\"mieng-dan-ppf-full-man-hinh-cao-cap-cho-samsung-galaxy-note-10-plus\",\"pro_price\":\"180000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":0,\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__9.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng D\\u00e1n PPF Full M\\u00e0n h\\u00ecnh Cao c\\u1ea5p cho Samsung Galaxy Note 10 Plus\",\"pro_content\":\"Mi\\u1ebfng D\\u00e1n PPF Full M\\u00e0n h\\u00ecnh Cao c\\u1ea5p cho Samsung Galaxy Note 10 Plus\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:49:20\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:55\"},\"new\":{\"id\":9,\"pro_name\":\"Mi\\u1ebfng D\\u00e1n PPF Full M\\u00e0n h\\u00ecnh Cao c\\u1ea5p cho Samsung Galaxy Note 10 Plus\",\"pro_slug\":\"mieng-dan-ppf-full-man-hinh-cao-cap-cho-samsung-galaxy-note-10-plus\",\"pro_price\":\"180000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":0,\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__9.jpg\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"Mi\\u1ebfng D\\u00e1n PPF Full M\\u00e0n h\\u00ecnh Cao c\\u1ea5p cho Samsung Galaxy Note 10 Plus\",\"pro_content\":\"Mi\\u1ebfng D\\u00e1n PPF Full M\\u00e0n h\\u00ecnh Cao c\\u1ea5p cho Samsung Galaxy Note 10 Plus\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 10:49:20\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 11:16:55\"}}', '2020-08-17 04:16:55', '2020-08-17 04:16:55'),
(16, 'Product', 'Cập nhật product', 1, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":1,\"pro_name\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_slug\":\"tai-nghe-bluetooth-apple-airpods-2-vna\",\"pro_price\":\"3650000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__12.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_content\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:34:19\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 15:27:49\"},\"new\":{\"id\":1,\"pro_name\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_slug\":\"tai-nghe-bluetooth-apple-airpods-2-vna\",\"pro_price\":\"3650000\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"2\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-08-17__12.webp\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"3\",\"pro_import_goods\":0,\"pro_description\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_content\":\"Tai nghe Bluetooth Apple AirPods 2 VN\\/A\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-08-17 09:34:19\",\"pro_expiration\":\"2020-08-17\",\"pro_number\":3,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-08-17 15:27:49\"}}', '2020-08-17 08:27:49', '2020-08-17 08:27:49'),
(17, 'Product', 'Cập nhật product', 85, 'App\\Models\\Product', 1, 'App\\Models\\Admin', '{\"old\":{\"id\":85,\"pro_name\":\"Tai nghe bluetooth M\\u00e8o\",\"pro_slug\":\"tai-nghe-bluetooth-meo\",\"pro_price\":\"1250\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"5\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-11-10__ff50a39b759ecd2c7e1395b79c1fa17d.png\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"Tai nghe bluetooth M\\u00e8o\",\"pro_content\":\"Tai nghe bluetooth M\\u00e8o\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-11-10 18:11:00\",\"pro_expiration\":\"2020-11-10\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-11-10 18:17:01\"},\"new\":{\"id\":85,\"pro_name\":\"Tai nghe bluetooth M\\u00e8o\",\"pro_slug\":\"tai-nghe-bluetooth-meo\",\"pro_price\":\"1250\",\"pro_price_entry\":0,\"pro_category_id\":\"28\",\"pro_supplier_id\":0,\"pro_admin_id\":0,\"pro_sale\":\"5\",\"pro_expiration_date\":10,\"pro_avatar\":\"2020-11-10__ff50a39b759ecd2c7e1395b79c1fa17d.png\",\"pro_view\":0,\"pro_hot\":0,\"pro_active\":1,\"pro_pay\":0,\"pro_number_import\":\"2\",\"pro_import_goods\":0,\"pro_description\":\"Tai nghe bluetooth M\\u00e8o\",\"pro_content\":\"Tai nghe bluetooth M\\u00e8o\",\"pro_review_total\":0,\"pro_review_star\":0,\"pro_age_review\":0,\"created_at\":\"2020-11-10 18:11:00\",\"pro_expiration\":\"2020-11-10\",\"pro_number\":2,\"pro_resistant\":null,\"pro_energy\":null,\"pro_country\":0,\"updated_at\":\"2020-11-10 18:17:01\"}}', '2020-11-10 11:17:01', '2020-11-10 11:17:01');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_login` text COLLATE utf8mb4_unicode_ci,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `log_login`, `class`, `address`, `level`, `status`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'doantotnghiep@gmail.com', '$2y$10$DRewqqh35y7Xp4HsgnrXGeAHp7XgV2QBgvkVAGnFuQKaRm6duHij2', '0988999999', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `a_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_hot` tinyint(4) NOT NULL DEFAULT '0',
  `a_active` tinyint(4) NOT NULL DEFAULT '1',
  `a_menu_id` int(11) NOT NULL DEFAULT '0',
  `a_view` int(11) NOT NULL DEFAULT '0',
  `a_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `a_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `a_position_2` tinyint(4) NOT NULL DEFAULT '0',
  `a_position_1` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `a_name`, `a_slug`, `a_hot`, `a_active`, `a_menu_id`, `a_view`, `a_description`, `a_avatar`, `a_content`, `created_at`, `a_position_2`, `a_position_1`, `updated_at`) VALUES
(1, 'Bài viết mới', 'bai-viet-moi', 1, 1, 1, 0, 'Bài viết mới', '2020-08-17__screen-shot-2020-08-17-at-125152-am.png', '<p>B&agrave;i viết mới</p>', '2020-08-16 20:14:16', 1, 1, '2020-08-16 20:14:42'),
(2, 'Bí quyết để thành công trong nghề kinh doanh bán phụ kiện điện thoại', 'bi-quyet-de-thanh-cong-trong-nghe-kinh-doanh-ban-phu-kien-dien-thoai', 1, 1, 2, 0, 'Đâu là cách mở cửa hàng phụ kiện điện thoại hiệu quả? Bạn cần phải có cho riêng mình một kế hoạch kinh doanh phụ kiện điện thoại càng cụ thể, chi tiết càng tốt. Hãy sẽ phải nắm rõ 11 yếu tố quan trọng sau đây cũng như những tư vấn, kinh nghiệm mở cửa hàng phụ kiện điện thoại 2020 hữu ích', '2020-08-17__kinh-doanh-phu-kien-dien-thoai-min-1.jpg', '<h3><strong>1.1. C&oacute; hiểu biết nhất định về phụ kiện điện thoại</strong></h3>\r\n\r\n<p><img alt=\"Để khởi nghiệp kinh doanh phụ kiện điện thoại, bạn cần những kiến thức nhất định về ngành\" src=\"https://sun-smart.vn/wp-content/uploads/kien-thuc-kinh-doanh-phu-kien-dien-thoai-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Để khởi nghiệp kinh doanh phụ kiện điện thoại, bạn cần những kiến thức nhất định về ng&agrave;nh</p>\r\n\r\n<p>Đầu ti&ecirc;n, h&atilde;y trang bị cho m&igrave;nh những kiến thức l&yacute; thuyết về ng&agrave;nh, những &yacute; tưởng kinh doanh, bu&ocirc;n b&aacute;n phụ kiện điện thoại hiệu quả. Bởi bạn đ&acirc;u thể cung cấp, tư vấn v&agrave; thuyết phục kh&aacute;ch h&agrave;ng mua sản phẩm của m&igrave;nh nếu ch&iacute;nh bạn cũng kh&ocirc;ng hề hiểu về phụ kiện điện thoại, đ&uacute;ng kh&ocirc;ng?</p>\r\n\r\n<p>Chưa kể đến, ng&agrave;nh điện thoại th&ocirc;ng minh ph&aacute;t triển cực kỳ nhanh ch&oacute;ng, mỗi năm c&oacute; đến h&agrave;ng trăm, h&agrave;ng ng&agrave;n c&aacute;c mẫu điện thoại mới được ra đời, k&eacute;o theo đ&oacute; l&agrave; c&aacute;c sản phẩm phụ kiện, hỗ trợ cũng đổi mới theo.</p>\r\n\r\n<p>Ch&iacute;nh v&igrave; thế, ở cương vị một người b&aacute;n, bạn cần t&igrave;m hiểu r&otilde; về c&aacute;c d&ograve;ng sản phẩm, c&aacute;c đời m&aacute;y, mỗi t&iacute;nh năng nổi trội&hellip; để c&oacute;&nbsp;&yacute; tưởng kinh doanh phụ kiện điện thoại tốt nhất.</p>\r\n\r\n<h3><strong>1.2. X&aacute;c định m&ocirc; h&igrave;nh, &yacute; tưởng kinh doanh phụ kiện điện thoại ph&ugrave; hợp</strong></h3>\r\n\r\n<p><img alt=\"Mở shop kinh doanh phụ kiện điện thoại cần bao nhiêu vốn?\" src=\"https://sun-smart.vn/wp-content/uploads/mo-shop-phu-kien-dien-thoai-can-bao-nhieu-von-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Mở shop kinh doanh phụ kiện điện thoại cần bao nhi&ecirc;u vốn?</p>\r\n\r\n<p>H&atilde;y phụ thuộc v&agrave;o nguồn vốn v&agrave; khả năng của m&igrave;nh m&agrave; lựa chọn được m&ocirc; h&igrave;nh, t&iacute;nh to&aacute;n c&aacute;c chi ph&iacute; khi mở cửa h&agrave;ng kinh doanh phụ kiện điện thoại ph&ugrave; hợp. Vậy&nbsp;mở shop kinh doanh phụ kiện điện thoại cần bao nhi&ecirc;u vốn?</p>\r\n\r\n<ul>\r\n	<li>Nếu bạn c&oacute; nguồn vốn lớn, ổn định (tầm tr&ecirc;n 100 triệu) th&igrave; h&atilde;y mở c&aacute;c cửa h&agrave;ng, đầu tư v&agrave;o nguồn h&agrave;ng lớn, mặt tiền rộng v&agrave; đội ngũ nh&acirc;n vi&ecirc;n chuy&ecirc;n nghiệp. L&uacute;c n&agrave;y kh&aacute;ch h&agrave;ng sẽ c&oacute; cơ hội lựa chọn trực tiếp sản phẩm v&agrave; tin tưởng hơn do bạn đ&atilde; c&oacute; mặt bằng. Ngo&agrave;i ra bạn c&ograve;n c&oacute; thể đầu tư v&agrave;o việc quảng c&aacute;o, khuyến mại để thu h&uacute;t th&ecirc;m kh&aacute;ch h&agrave;ng.</li>\r\n	<li>Nguồn vốn vừa phải (50 &ndash; 100 triệu) bạn c&oacute; thể chọn mở c&aacute;c cửa h&agrave;ng c&oacute; diện t&iacute;ch nhỏ hơn, chấp nhận đầu tư nhỏ, sau khi ổn định v&agrave; c&oacute; lợi nhuận th&igrave; c&acirc;n nhắc việc mở rộng.</li>\r\n	<li>Nếu bạn chỉ c&oacute; nguồn vốn ban đầu &iacute;t (30 &ndash; 50 triệu) th&igrave; đừng lo lắng, bạn c&oacute; thể bu&ocirc;n b&aacute;n online trước, thay v&igrave; đầu tư v&agrave;o mặt bằng, h&atilde;y d&ugrave;ng tiền vốn để nhập h&agrave;ng v&agrave; quảng c&aacute;o tr&ecirc;n c&aacute;c trang mạng x&atilde; hội.</li>\r\n</ul>\r\n\r\n<h3><strong>1.3. X&aacute;c định địa điểm ph&ugrave; hợp để&nbsp;mở cửa h&agrave;ng kinh doanh phụ kiện điện thoại</strong></h3>\r\n\r\n<p><img alt=\"Tìm địa điểm mở cửa hàng là bước khá &quot;đau đầu&quot; khi bắt đầu dự án kinh doanh phụ kiện điện thoại\" src=\"https://sun-smart.vn/wp-content/uploads/tim-dia-diem-kinh-doanh-phu-hop-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>T&igrave;m địa điểm mở cửa h&agrave;ng l&agrave; bước kh&aacute; &ldquo;đau đầu&rdquo; khi bắt đầu dự &aacute;n kinh doanh phụ kiện điện thoại</p>\r\n\r\n<p>C&oacute; thể n&oacute;i, địa điểm kinh doanh quyết định đến 50% th&agrave;nh c&ocirc;ng của cửa h&agrave;ng phụ kiện điện thoại. V&igrave; thế, nếu c&oacute; thể h&atilde;y lựa chọn c&aacute;c địa điểm c&oacute; nhiều đối tượng l&agrave; kh&aacute;ch h&agrave;ng mục ti&ecirc;u của bạn.</p>\r\n\r\n<p>Với mặt h&agrave;ng phụ kiện điện thoại, kh&aacute;ch h&agrave;ng chủ yếu sẽ l&agrave; người trẻ tuổi, phổ biến nhất l&agrave; dưới 25 tuổi, n&ecirc;n ph&ugrave; hợp nhất l&agrave; gần c&aacute;c trường đại học, cao đẳng, tại khu vực sầm uất hoặc gần trung t&acirc;m&hellip; l&agrave; l&yacute; tưởng nhất.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, việc chọn địa điểm để mở shop phụ kiện điện thoại cũng phụ thuộc v&agrave;o rất nhiều yếu tố như diện t&iacute;ch mặt bằng, vị tr&iacute; trong ng&otilde; hay mặt đường, mức độ cạnh tranh tại khu vực đ&oacute;,&hellip; bạn h&atilde;y c&acirc;n nhắc dựa tr&ecirc;n số vốn của m&igrave;nh để c&oacute; sự lựa chọn tốt nhất.</p>\r\n\r\n<h3><strong>1.4. Nghi&ecirc;n cứu thị trường, đối thủ cạnh tranh</strong></h3>\r\n\r\n<p><img alt=\"Bước tiếp theo để xây dựng dự án kinh doanh phụ kiện điện thoại tốt là nghiên cứu đối thủ.\" src=\"https://sun-smart.vn/wp-content/uploads/nghien-cuu-doi-thu-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Bước tiếp theo để x&acirc;y dựng dự &aacute;n kinh doanh phụ kiện điện thoại tốt l&agrave; nghi&ecirc;n cứu đối thủ.</p>\r\n\r\n<p>Kinh doanh phụ kiện điện thoại l&agrave; mảnh đất m&agrave;u mỡ, v&igrave; thế mức độ cạnh tranh cũng tương đối cao, v&igrave; thế để chuẩn bị thật tốt, bạn cũng cần phải c&oacute; những nghi&ecirc;n cứu, ph&acirc;n t&iacute;ch cụ thể.</p>\r\n\r\n<p>Bạn n&ecirc;n tiến h&agrave;nh t&igrave;m hiểu những yếu tố như d&acirc;n cư trong v&ugrave;ng, độ tuổi trung b&igrave;nh của khu vực đ&oacute;, mức thu nhập trung b&igrave;nh hay xu hướng mua h&agrave;ng&hellip; để c&oacute; thể nắm được th&oacute;i quen ti&ecirc;u d&ugrave;ng của kh&aacute;ch h&agrave;ng mục ti&ecirc;u. Từ đ&oacute; c&oacute; được sự lựa chọn mặt h&agrave;ng ph&ugrave; hợp nhất.</p>\r\n\r\n<p>Về đối thủ cạnh tranh, bạn cần t&igrave;m hiểu v&agrave; ph&acirc;n t&iacute;ch về gi&aacute; b&aacute;n, mẫu m&atilde;, thời gian mở, đ&oacute;ng cửa h&agrave;ng&hellip; để c&oacute; được chiến lược ph&ugrave; hợp nhất. Lưu &yacute; rằng bạn n&ecirc;n xem x&eacute;t cả c&aacute;c đối thủ l&agrave; những cửa h&agrave;ng b&aacute;n phụ kiện điện thoại, v&agrave; cửa h&agrave;ng b&aacute;n điện thoại c&oacute; k&egrave;m phụ kiện&hellip;</p>\r\n\r\n<p>Nếu may mắn bạn c&ograve;n sẽ c&oacute; được những mối quan hệ tốt, những người sẽ tư vấn, hướng dẫn bạn những bước đầu khởi nghiệp kinh doanh phụ kiện điện thoại.</p>\r\n\r\n<h3><strong>1.5. X&aacute;c định mặt h&agrave;ng kinh doanh chủ đạo</strong></h3>\r\n\r\n<p><img alt=\"Kinh nghiệm mở shop phụ kiện điện thoại cho thấy, bạn cần xác định mặt hàng chủ đạo ngay từ đầu\" src=\"https://sun-smart.vn/wp-content/uploads/mat-hang-kinh-doanh-chu-dao-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Kinh nghiệm mở shop phụ kiện điện thoại cho thấy, bạn cần x&aacute;c định mặt h&agrave;ng chủ đạo ngay từ đầu</p>\r\n\r\n<p>Ng&agrave;nh phụ kiện điện thoại c&oacute; rất đa dạng sản phẩm như: ốp lưng, tai nghe, miếng d&aacute;n điện thoại, gậy tự sướng,&hellip; vậy n&ecirc;n bạn cần x&aacute;c định được m&igrave;nh &yacute; tưởng ngay từ đầu sẽ kinh doanh mặt h&agrave;ng n&agrave;o l&agrave; chủ đạo trước khi mở shop phụ kiện điện thoại.</p>\r\n\r\n<ul>\r\n	<li>Nếu bạn c&oacute; nguồn vốn đầu tư ban đầu lớn, th&igrave; sẽ dễ d&agrave;ng hơn trong việc lựa chọn mặt h&agrave;ng kinh doanh chủ đạo, hoặc bạn c&oacute; thể nhập c&agrave;ng đa dạng mẫu m&atilde; c&agrave;ng thu h&uacute;t kh&aacute;ch h&agrave;ng, hoặc nhập th&ecirc;m những sản phẩm mới lạ hơn như: loa đ&agrave;i, kẹp điện thoại, d&acirc;y treo điện thoại, bộ k&iacute;ch s&oacute;ng, tay cầm chơi game&hellip;</li>\r\n	<li>Tuy nhi&ecirc;n, nếu bạn c&oacute; nguồn vốn hạn hẹp th&igrave; h&atilde;y nghi&ecirc;n cứu v&agrave; lựa chọn mặt h&agrave;ng kinh doanh thật kỹ lưỡng, h&atilde;y t&igrave;m kiếm những mặt h&agrave;ng đang được kh&aacute;ch h&agrave;ng quan t&acirc;m nhiều, khả năng ti&ecirc;u thụ nhanh, quay v&ograve;ng vốn nhanh&hellip; th&igrave; mới đảm bảo việc kinh doanh thuận lợi. Những mặt h&agrave;ng như ốp lưng, tai nghe, sạc dự ph&ograve;ng&hellip; th&igrave; lu&ocirc;n c&oacute; một lượng lớn kh&aacute;ch h&agrave;ng quan t&acirc;m, bạn c&oacute; thể xem x&eacute;t lựa chọn đ&acirc;u l&agrave; mặt h&agrave;ng chủ đạo ph&ugrave; hợp nhất.</li>\r\n</ul>\r\n\r\n<h3><strong>2.6. T&igrave;m nguồn nhập phụ kiện điện thoại chất lượng v&agrave; gi&aacute; hợp l&yacute;</strong></h3>\r\n\r\n<p><img alt=\"Kinh doanh phụ kiện điện thoại cần những gì? Nguồn hàng sỉ là một trong những yếu tố quan trọng nhất?\" src=\"https://sun-smart.vn/wp-content/uploads/tai-sao-nen-lua-chon-kinh-doanh-phu-kien-dien-thoai-o-nghe-an-thoi-diem-hien-tai-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Kinh doanh phụ kiện điện thoại cần những g&igrave;? Nguồn h&agrave;ng sỉ l&agrave; một trong những yếu tố quan trọng nhất?</p>\r\n\r\n<p>Muốn kinh doanh phụ kiện điện thoại c&oacute; l&atilde;i, t&igrave;m nguồn h&agrave;ng sỉ rẻ v&agrave; chất lượng l&agrave; điều m&agrave; chủ cửa h&agrave;ng n&agrave;o cũng quan t&acirc;m. Bạn c&oacute; thể dễ d&agrave;ng nhập h&agrave;ng từ:</p>\r\n\r\n<ul>\r\n	<li>Nhập h&agrave;ng từ nội địa Trung Quốc, tại c&aacute;c chợ như Quảng Ch&acirc;u, Th&acirc;m Quyến&hellip; Ưu điểm l&agrave; mẫu m&atilde; h&agrave;ng đa dạng, cập nhật xu hướng, gi&aacute; rẻ, bạn c&oacute; thể thoải m&aacute;i lựa chọn. Tuy nhi&ecirc;n c&aacute;ch n&agrave;y chỉ ph&ugrave; hợp với ai c&oacute; kinh nghiệm, bạn sẽ phải đối mặt với vấn đề nhập cảnh v&agrave; nhất l&agrave; giao tiếp, trả gi&aacute;&hellip; Ngo&agrave;i ra, nhiều người chọn nhập h&agrave;ng online tại H&agrave;n Quốc hay Nhật Bản để đảm bảo chất lượng với mức gi&aacute; cao hơn nhiều.</li>\r\n	<li>Nhập h&agrave;ng từ chợ đầu mối như như chợ M&oacute;ng C&aacute;i, Lạng Sơn&hellip; Ưu điểm l&agrave; kh&ocirc;ng cần lo vấn đề giao tiếp, nhập cảnh. Mức gi&aacute; tại đ&acirc;y cũng rẻ, tuy nhi&ecirc;n chất lượng kh&ocirc;ng đồng đều, hay th&aacute;ch gi&aacute;, bạn phải c&oacute; kinh nghiệm chọn h&agrave;ng v&agrave; mặc cả.</li>\r\n	<li>Nhập h&agrave;ng từ đại l&yacute;, nh&agrave; ph&acirc;n phối uy t&iacute;n trong nước như&nbsp;<a href=\"https://sun-smart.vn/\">SunSmart</a>,&nbsp;<a href=\"https://phukienahai.vn/\">Phụ kiện A Hải</a>&hellip; Đ&acirc;y l&agrave; h&igrave;nh thức m&agrave; phần lớn c&aacute;c cửa h&agrave;ng b&aacute;n lẻ chọn. Gi&aacute; nhỉnh hơn ch&uacute;t so với 2 h&igrave;nh thức tr&ecirc;n, nhưng bạn sẽ tiết kiệm được thời gian, c&ocirc;ng sức v&agrave; đặc biệt l&agrave; đảm bảo chất lượng. Bạn c&ograve;n được hưởng ch&iacute;nh s&aacute;ch đổi trả, bảo h&agrave;nh r&otilde; r&agrave;ng v&agrave; đặc biệt l&agrave; c&aacute;c chương tr&igrave;nh ưu đ&atilde;i.</li>\r\n</ul>\r\n\r\n<p><em>C&oacute; thể bạn quan t&acirc;m:</em></p>\r\n\r\n<p><a href=\"https://sun-smart.vn/tin-tuc/lay-si-phu-kien-dien-thoai-o-dau.html\" target=\"_blank\">Xem th&ecirc;m:&nbsp;&nbsp;&nbsp;Mua phụ kiện điện thoại gi&aacute; sỉ ở đ&acirc;u? &quot;BẬT M&Iacute;&quot; 3 nguồn uy t&iacute;n</a></p>\r\n\r\n<h3><strong>2.7. Trang tr&iacute; cửa h&agrave;ng phụ kiện điện thoại</strong></h3>\r\n\r\n<p><img alt=\"Mở cửa hàng phụ kiện điện thoại cần những gì? Đừng bỏ qua việc trang hoàng cho shop\" src=\"https://sun-smart.vn/wp-content/uploads/trang-tri-cua-hang-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Mở cửa h&agrave;ng phụ kiện điện thoại cần những g&igrave;? Đừng bỏ qua việc trang ho&agrave;ng cho shop</p>\r\n\r\n<p>Về việc trang tr&iacute; cửa h&agrave;ng, ng&agrave;nh phụ kiện điện thoại kh&ocirc;ng qu&aacute; khắt khe như c&aacute;c ng&agrave;nh h&agrave;ng kh&aacute;c, cũng kh&ocirc;ng cần qu&aacute; đầu tư, ng&acirc;n s&aacute;ch cho phần trang tr&iacute; kh&ocirc;ng n&ecirc;n chiếm qu&aacute; 20% vốn mở cửa h&agrave;ng phụ kiện điện thoại của bạn. Nhưng h&atilde;y lưu &yacute; những điều sau:</p>\r\n\r\n<ul>\r\n	<li>Cần chuẩn bị nhiều c&aacute;c kệ trưng b&agrave;y, gi&aacute; treo phụ kiện&hellip; để dễ d&agrave;ng v&agrave; thuận tiện cho kh&aacute;ch h&agrave;ng khi xem v&agrave; lựa chọn.</li>\r\n	<li>N&ecirc;n ph&acirc;n loại c&aacute;c khu vực để gi&uacute;p kh&aacute;ch dễ d&agrave;ng t&igrave;m được sản phẩm cần thiết, khu vực ốp lưng, pin dự ph&ograve;ng, d&acirc;y c&aacute;p&hellip; n&ecirc;n được để ri&ecirc;ng v&agrave; c&aacute;ch biệt.</li>\r\n	<li>H&atilde;y ch&uacute; &yacute; hơn khi thiết kế biển hiệu v&agrave; trang ho&agrave;ng cửa h&agrave;ng, l&agrave;m sao để kh&aacute;ch h&agrave;ng c&oacute; thể nhận ra bạn đang b&aacute;n phụ kiện điện thoại v&agrave; bị thu h&uacute;t ngay lập tức.</li>\r\n	<li>Đặc biệt, một cửa h&agrave;ng với thiết kế độc đ&aacute;o sẽ l&agrave; b&iacute; quyết thu h&uacute;t kh&aacute;ch h&agrave;ng gi&uacute;p kinh doanh phụ kiện điện thoại tốt hơn, như hệ thống chiếu s&aacute;ng chẳng hạn&hellip;</li>\r\n</ul>\r\n\r\n<h3><strong>2.8. Đăng k&yacute; giấy ph&eacute;p kinh doanh shop phụ kiện điện thoại</strong></h3>\r\n\r\n<p>Một trong những&nbsp;thủ tục mở cửa h&agrave;ng phụ kiện điện thoại&nbsp;bạn bắt buộc phải l&agrave;m l&agrave; đăng k&yacute; giấy ph&eacute;p kinh doanh phụ kiện điện thoại.</p>\r\n\r\n<p><img alt=\"Đăng ký giấy phép trước khi kinh doanh phụ kiện điện thoại là thủ tục bắt buộc\" src=\"https://sun-smart.vn/wp-content/uploads/giay-phep-kinh-doanh-phu-kien-dien-thoai-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Đăng k&yacute; giấy ph&eacute;p trước khi kinh doanh phụ kiện điện thoại l&agrave; thủ tục bắt buộc</p>\r\n\r\n<p>Hiện nay, nhiều người cho rằng mở kinh doanh nhỏ th&igrave; kh&ocirc;ng cần phải l&agrave;m giấy ph&eacute;p, đ&acirc;y ho&agrave;n to&agrave;n l&agrave; sai lầm v&agrave; sẽ dễ d&agrave;ng khiến bạn vướng phải rắc rối với ch&iacute;nh quyền địa phương.</p>\r\n\r\n<p>Thủ tục để đăng k&yacute; giấy ph&eacute;p kinh doanh phụ kiện điện thoại kh&ocirc;ng hề kh&oacute; khăn, bạn c&oacute; thể t&igrave;m hiểu kinh nghiệm của những người đi trước, hay thậm ch&iacute; t&igrave;m đến c&aacute;c đơn vị trung gian l&agrave;m &ldquo;dịch vụ&rdquo; để tiết kiệm c&ocirc;ng sức, thời gian.</p>\r\n\r\n<h3><strong>2.9. X&acirc;y dựng chiến lược Marketing &ndash; Quảng b&aacute; cửa h&agrave;ng</strong></h3>\r\n\r\n<p><img alt=\"Hãy dành ra một phần vốn kinh doanh phụ kiện điện thoại để xây dựng chiến lược marketing - quảng bá.\" src=\"https://sun-smart.vn/wp-content/uploads/xay-dung-chien-luoc-marketing-quang-ba-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>H&atilde;y d&agrave;nh ra một phần vốn kinh doanh phụ kiện điện thoại để x&acirc;y dựng chiến lược marketing &ndash; quảng b&aacute;.</p>\r\n\r\n<p>C&oacute; một kế hoạch, chiến lược quảng b&aacute; cho cửa h&agrave;ng l&agrave; điều cần thiết nếu bạn muốn thu h&uacute;t th&ecirc;m kh&aacute;ch h&agrave;ng, mở rộng quy m&ocirc;&hellip; H&atilde;y t&iacute;nh đến những kế hoạch quảng b&aacute; tr&ecirc;n c&aacute;c k&ecirc;nh truyền th&ocirc;ng, đồng thời thiết lập l&ecirc;n c&aacute;c chương tr&igrave;nh khuyến mại c&agrave;ng chi tiết c&agrave;ng tốt.</p>\r\n\r\n<p>Hiện nay c&oacute; rất nhiều c&aacute;ch để quảng b&aacute;, kinh doanh bu&ocirc;n b&aacute;n phụ kiện điện thoại m&agrave; bạn n&ecirc;n ch&uacute; &yacute;:</p>\r\n\r\n<ul>\r\n	<li>Website: gi&uacute;p bạn n&acirc;ng cao uy t&iacute;n cho cửa h&agrave;ng, tiếp cận được nhiều tập kh&aacute;ch h&agrave;ng hơn.</li>\r\n	<li>Mạng x&atilde; hội như: facebook, zalo, instagram&hellip; l&agrave; k&ecirc;nh gi&uacute;p bạn dễ d&agrave;ng tiếp cận th&ecirc;m c&aacute;c kh&aacute;ch h&agrave;ng trẻ, h&atilde;y thường xuy&ecirc;n cập nhật c&aacute;c sản phẩm mới, mẫu m&atilde; độc đ&aacute;o để k&iacute;ch th&iacute;ch kh&aacute;ch h&agrave;ng mua sắm.</li>\r\n	<li>Chương tr&igrave;nh khuyến mại: Những kế hoạch cần c&oacute; l&agrave; về c&aacute;c chương tr&igrave;nh khuyến m&atilde;i, ưu đ&atilde;i trong c&aacute;c dịp đặc biệt như khai trương, ng&agrave;y lễ&hellip; để thu h&uacute;t kh&aacute;ch h&agrave;ng.</li>\r\n</ul>\r\n\r\n<h3><strong>2.10. Li&ecirc;n tục quản l&yacute; v&agrave; điều h&agrave;nh dự &aacute;n kinh doanh phụ kiện điện thoại</strong></h3>\r\n\r\n<p><img alt=\"Cách để giữ việc kinh doanh, buôn bán phụ kiện điện thoại của bạn đi đúng hương: Quản lý liên tục\" src=\"https://sun-smart.vn/wp-content/uploads/cach-quan-ly-shop-phu-kien-dien-thoai-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>C&aacute;ch để giữ việc kinh doanh, bu&ocirc;n b&aacute;n phụ kiện điện thoại của bạn đi đ&uacute;ng hương: Quản l&yacute; li&ecirc;n tục</p>\r\n\r\n<p>D&ugrave; l&agrave; với quy m&ocirc; nhỏ hay lớn th&igrave; việc quản l&yacute; v&agrave; điều h&agrave;nh l&agrave; việc rất quan trọng. L&uacute;c n&agrave;y bạn cần phải c&oacute; những kỹ năng quản l&yacute; nhất định, h&atilde;y tự học hỏi th&ecirc;m v&agrave; t&igrave;m hỏi kinh nghiệm của những người đi trước.</p>\r\n\r\n<p>H&atilde;y c&oacute; những ch&iacute;nh s&aacute;ch, bản quy định chung cho nh&acirc;n vi&ecirc;n của bạn, ghi r&otilde; r&agrave;ng về lịch l&agrave;m việc, kỹ năng cần c&oacute;, c&aacute;ch giao tiếp với kh&aacute;ch h&agrave;ng, ghi ch&eacute;p h&agrave;ng h&oacute;a&hellip; v&agrave; c&aacute;c mức lương thưởng để đảm bảo minh bạch v&agrave; chăm s&oacute;c tốt nhất cho kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>Nếu cửa h&agrave;ng c&oacute; quy m&ocirc; lớn, việc nhập v&agrave; b&aacute;n h&agrave;ng kh&oacute; kiểm so&aacute;t th&igrave; bạn c&oacute; thể đầu tư việc sử dụng c&aacute;c phần mềm b&aacute;n h&agrave;ng để tiện lợi hơn, vừa dễ d&agrave;ng quản l&yacute; lại c&oacute; thể kiểm k&ecirc; được l&atilde;i lỗ nhanh ch&oacute;ng.</p>\r\n\r\n<p>Một mảng v&ocirc; c&ugrave;ng quan trọng kh&aacute;c bạn cần quan t&acirc;m l&agrave; giữ cho d&ograve;ng vốn kinh doanh phụ kiện điện thoại lưu chuyển hợp l&yacute;.</p>\r\n\r\n<h3><strong>2.11. Lập kế hoạch kinh doanh phụ kiện điện thoại chi tiết v&agrave; tỉ mỉ</strong></h3>\r\n\r\n<p><img alt=\"Một trong những bí quyết kinh doanh phụ kiện điện thoại quan trọng nhất là LẬP KẾ HOẠCH\" src=\"https://sun-smart.vn/wp-content/uploads/bi-quyet-kinh-doanh-phu-kien-dien-thoai-600x450.jpg\" style=\"height:450px; width:600px\" /></p>\r\n\r\n<p>Một trong những b&iacute; quyết kinh doanh phụ kiện điện thoại quan trọng nhất l&agrave; LẬP KẾ HOẠCH</p>\r\n\r\n<p>Sau khi đ&atilde; nắm được hết c&aacute;c lưu &yacute; tr&ecirc;n, h&atilde;y t&igrave;m hiểu, ghi ch&uacute; lại v&agrave; lập l&ecirc;n cho m&igrave;nh một kế hoạch kinh doanh phụ kiện điện thoại chi tiết v&agrave; tỉ mỉ nhất. Bởi c&oacute; kế hoạch, bạn sẽ nh&igrave;n r&otilde; được bức tranh to&agrave;n cảnh, từ đ&oacute; c&oacute; thể ph&acirc;n bổ nguồn vốn vốn kinh doanh phụ kiện điện thoại hợp l&yacute;, tr&aacute;nh được rủi ro v&agrave; dễ d&agrave;ng ph&aacute;t triển bền vững.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, kh&ocirc;ng phải ai cũng c&oacute; sẵn những kinh nghiệm lập kế hoạch kinh doanh, cụ thể l&agrave; mặt h&agrave;ng phụ kiện điện thoại n&agrave;y, v&igrave; thế đừng ngần ngại m&agrave; học hỏi, t&igrave;m hiểu những kinh nghiệm của người đi trước, hoặc thậm ch&iacute; đăng k&yacute; c&aacute;c lớp học về quản trị v&agrave; lập kế hoạch trước khi kinh doanh. Điều n&agrave;y l&agrave; để đảm bảo bạn sẽ ph&acirc;n t&iacute;ch được thị trường, c&oacute; khả năng nh&igrave;n thấy lường trước mọi vấn đề ph&aacute;t sinh v&agrave; đưa ra c&aacute;c phương &aacute;n giải quyết c&aacute;c vấn đề&hellip;</p>', '2020-08-17 08:55:26', 0, 1, '2020-08-17 10:36:56'),
(3, 'Kinh doanh phụ kiện điện thoại: Vốn nhỏ, rủi ro ít, lãi cao', 'kinh-doanh-phu-kien-dien-thoai-von-nho-rui-ro-it-lai-cao', 1, 1, 2, 0, 'Khi thời đại công nghệ bùng nổ, kinh doanh phụ kiện điện thoại là ý tưởng vô cùng hấp dẫn. Dưới đây Sapo chia sẻ kinh nghiệm hay hỗ trợ bạn kinh doanh phụ kiện điện thoại tốt hơn.', '2020-08-17__co-nen-kinh-doanh-phu-kien-dien-thoai-8.jpg', '<p>&nbsp;</p>\r\n\r\n<p>Theo thống k&ecirc; ở Việt Nam, 55% d&acirc;n số đ&atilde; sử dụng điện thoại th&ocirc;ng minh v&agrave; kết nối Internet. R&otilde; r&agrave;ng, mobile đang l&agrave; mảnh đất m&agrave;u mỡ m&agrave; bất cứ doanh nghiệp n&agrave;o cũng kh&ocirc;ng n&ecirc;n bỏ qua. Sự ph&aacute;t triển của điện thoại th&ocirc;ng minh k&eacute;o theo &ldquo;những ng&agrave;nh ăn theo&rdquo; như phụ kiện cũng dễ d&agrave;ng &ldquo;hốt bạc triệu&rdquo;. Nếu bạn đang nhen nh&uacute;m &yacute; định mở shop b&aacute;n h&agrave;ng về lĩnh vực phụ kiện smartphone nhanh nhanh tham khảo những kinh nghiệm dưới đ&acirc;y nh&eacute;. Chắc chắn bạn sẽ c&oacute; th&ecirc;m nhiều &yacute; tưởng để sẵn s&agrave;ng lấn s&acirc;n v&agrave;o c&ocirc;ng việc n&agrave;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>1. C&oacute; hiểu biết về phụ kiện điện thoại</h2>\r\n\r\n<p>Phụ kiện cho điện thoại kh&ocirc;ng chỉ đ&ograve;i hỏi về mặt thời trang, m&agrave; c&ograve;n phải bền, hợp t&uacute;i tiền, mang lại nhiều tiện &iacute;ch. Đa phần mọi người thường t&igrave;m mua phụ kiện cho &ldquo;dế y&ecirc;u&rdquo; đều biết c&aacute;ch lựa chọn sản phẩm hội tụ đầy đủ c&aacute;c ti&ecirc;u ch&iacute;. Chưa kể đến việc, sự ph&aacute;t triển c&ocirc;ng nghệ như vũ b&atilde;o th&uacute;c đầy sự ra đời li&ecirc;n tiếp của nhiều d&ograve;ng smartphone mới. Muốn kinh doanh phụ kiện điện thoại, bạn nhất định phải c&oacute; hiểu biết về c&aacute;c đời m&aacute;y, t&iacute;nh năng của từng d&ograve;ng để tư vấn kh&aacute;ch h&agrave;ng mua phụ kiện.</p>\r\n\r\n<h2>2. Kinh doanh phụ kiện điện thoại cần bao nhi&ecirc;u vốn?</h2>\r\n\r\n<p>Lần trước, Sapo đ&atilde; chia sẻ với c&aacute;c bạn về l&yacute; do n&ecirc;n chọn&nbsp;<a href=\"https://www.sapo.vn/blog/nhung-ly-nen-chon-phu-kien-dien-thoai-de-kinh-doanh-nho/\">kinh doanh phụ kiện điện thoại</a>. Kinh doanh phụ kiện điện thoại cần bao nhi&ecirc;u vốn l&agrave; do bản th&acirc;n bạn quyết định. Nếu muốn mở shop quy m&ocirc; lớn, c&oacute; thu&ecirc; nh&acirc;n vi&ecirc;n th&igrave; cần nhiều vốn. C&ograve;n nếu chỉ c&oacute; &iacute;t vốn th&igrave; ngại g&igrave; m&agrave; kh&ocirc;ng<a href=\"https://www.sapo.vn/bang-gia.html?&amp;utm_campaign=cpn:ref_random-plm:article&amp;utm_source=blog&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=blog_ref_random\" target=\"_blank\">&nbsp;tạo lập một website b&aacute;n phụ kiện online</a>&nbsp;rồi mới mở cửa h&agrave;ng nhỉ? B&aacute;n quy m&ocirc; nhỏ hoặc kinh doanh phụ kiện điện thoại online th&igrave; cần &iacute;t vốn hơn nh&eacute;.</p>\r\n\r\n<ul>\r\n	<li>Từ 100 &ndash; 200 triệu: Nếu bạn sở hữu trong tay từng n&agrave;y vốn th&igrave; rất tuyệt vời. Bạn chỉ cần bỏ ra 30 &ndash; 50 triệu để lấy h&agrave;ng gồm kh&aacute; nhiều loại phụ kiện điện thoại từ: ốp lưng, ốp da, k&iacute;nh cường lực, gậy tự sướng, tai nghe, loa, sạc&hellip; Phần vốn c&ograve;n lại bạn c&oacute; thể chi trả cho thu&ecirc; mặt bằng, đặt cọc, mua sắm gi&aacute; treo đồ, thu&ecirc; nh&acirc;n vi&ecirc;n, quảng c&aacute;o, l&agrave;m website, fanpage,&hellip;</li>\r\n	<li>Vốn từ 50 &ndash; 100 triệu: Bạn chỉ cần bỏ ra khoảng 15 &ndash; 20 triệu l&agrave; đ&atilde; c&oacute; thể lấy được h&agrave;ng với đủ c&aacute;c m&oacute;n phụ kiện, đ&aacute;p ứng cho những d&ograve;ng m&aacute;y cơ bản c&oacute; mặt tr&ecirc;n thị trường. Bạn cũng kh&ocirc;ng cần phải lấy qu&aacute; nhiều, chỉ cần chọn những m&oacute;n đang được ưa th&iacute;ch tr&ecirc;n thị trường, những phụ kiện cho c&aacute;c d&ograve;ng m&aacute;y hay được sử dụng nhiều như Samsung, iPhone. Ngo&agrave;i ra, số vốn c&ograve;n lại bạn cần l&ecirc;n kế hoạch t&igrave;m địa điểm thu&ecirc; rẻ, nhỏ để đ&aacute;p ứng được mục ti&ecirc;u hoạt động của shop.</li>\r\n	<li>Vốn từ 20-50 triệu: Với mức vốn đầu tư kh&aacute; khi&ecirc;m tốn như thế n&agrave;y, bạn cần phải nghi&ecirc;n cứu r&otilde; thị trường cũng như mục ti&ecirc;u của m&igrave;nh. Phải t&igrave;m hiểu tr&ecirc;n thị trường hiện nay người d&ugrave;ng th&iacute;ch d&ograve;ng sản phẩm n&agrave;o để chủ động lấy h&agrave;ng. Chi ph&iacute; lấy h&agrave;ng ban đầu c&oacute; thể rơi v&agrave;o khoảng 10 &ndash; 15 triệu. Nếu kh&ocirc;ng c&oacute; điều kiện thu&ecirc; mặt bằng, bạn c&oacute; thể chọn kinh doanh phụ kiện điện thoại online để giảm bớt chi ph&iacute;.</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://i2.wp.com/www.sapo.vn/blog/wp-content/uploads/2017/08/kinh-doanh-phu-kien-dien-thoai-min-1.jpg?resize=680%2C527&amp;quality=100&amp;strip=all&amp;ssl=1\" style=\"height:512px; width:660px\" /></p>\r\n\r\n<p><em>Kinh doanh phụ kiện điện thoại l&agrave; tr&agrave;o lưu năm 2017</em></p>\r\n\r\n<h2>3. N&ecirc;n chọn địa điểm kinh doanh phụ kiện điện thoại ở đ&acirc;u l&agrave; th&iacute;ch hợp?</h2>\r\n\r\n<p>Kinh nghiệm kinh doanh phụ kiện điện thoại th&agrave;nh c&ocirc;ng ch&iacute;nh l&agrave; biết chọn địa điểm th&iacute;ch hợp. Bạn c&oacute; thể chọn địa điểm tại c&aacute;c chợ sinh vi&ecirc;n lớn như: chợ Xanh Cầu Giấy, chợ Ph&ugrave;ng Khoang, chợ Đồng Xu&acirc;n,&hellip; Mở shop gần c&aacute;c trường đại học, cao đẳng, c&aacute;c khu đ&ocirc;ng d&acirc;n cư. V&igrave; đ&acirc;y l&agrave; mặt h&agrave;ng gi&aacute; rẻ, th&iacute;ch hợp cho đối tượng trẻ n&ecirc;n chủ yếu được sinh vi&ecirc;n ưa chuộng nhiều. Nếu kh&ocirc;ng c&oacute; điều kiện thu&ecirc; shop ở đường ch&iacute;nh th&igrave; c&oacute; thể chọn trong ng&otilde; nhưng đ&ocirc;ng d&acirc;n cư, đ&ocirc;ng sinh vi&ecirc;n thu&ecirc; trọ cũng rất hợp l&yacute;.</p>\r\n\r\n<h2>4. Chọn nguồn h&agrave;ng cho kinh doanh phụ kiện điện thoại thế n&agrave;o?</h2>\r\n\r\n<p>Hiện nay c&oacute; rất nhiều<a href=\"https://www.sapo.vn/blog/tim-nguon-hang-phu-kien-dien-thoai-o-dau/\">&nbsp;nguồn h&agrave;ng phong ph&uacute;, đa dạng</a>&nbsp;gi&uacute;p bạn chọn lựa.</p>\r\n\r\n<ul>\r\n	<li>Chợ đầu mối: Một số chợ đầu mối phụ kiện điện thoại nổi tiếng m&agrave; bạn c&oacute; thể t&igrave;m kiếm nguồn h&agrave;ng như: chợ Kim Bi&ecirc;n, chợ An Đ&ocirc;ng, chợ B&igrave;nh T&acirc;y (HCM), chợ Ph&ugrave;ng Khoang, chợ Trời (H&agrave; Nội)&hellip; Đến những chợ n&agrave;y, bạn sẽ hoa mắt v&igrave; lượng h&agrave;ng h&oacute;a khổng lồ, gi&aacute; rẻ m&agrave; lại phong ph&uacute;. Tuy nhi&ecirc;n, nhược điểm ở c&aacute;c chợ đầu mối l&agrave; h&agrave;ng tốt h&agrave;ng nh&aacute;i tr&agrave;n lan kh&oacute; ph&acirc;n biệt, nếu kh&ocirc;ng c&oacute; kinh nghiệm sẽ bị &ldquo;ch&eacute;m gi&aacute;&rdquo;. Tốt nhất n&ecirc;n chọn người c&oacute; kinh nghiệm đi c&ugrave;ng để tr&aacute;nh bị mua hớ nh&eacute;!</li>\r\n	<li>Đại l&yacute; bu&ocirc;n &ndash; sỉ: Bạn c&oacute; thể t&igrave;m c&aacute;c nh&agrave; bu&ocirc;n sỉ phụ kiện điện thoại tr&ecirc;n mạng, c&aacute;c diễn đ&agrave;n lớn. C&aacute;c nh&agrave; bu&ocirc;n, c&ocirc;ng ty b&aacute;n bu&ocirc;n đều c&oacute; website, fanpage ri&ecirc;ng n&ecirc;n bạn dễ d&agrave;ng li&ecirc;n hệ v&agrave; trao đổi về gi&aacute; cả, ch&iacute;nh s&aacute;ch hợp t&aacute;c. &nbsp;N&ecirc;n tham khảo nhiều nguồn để c&oacute; được gi&aacute; cả hợp l&yacute; nhất.</li>\r\n	<li>C&aacute;c cửa h&agrave;ng phụ kiện điện thoại lớn: H&atilde;y tới c&aacute;c cửa h&agrave;ng phụ kiện điện thoại lớn v&agrave; đề cập vấn đề hợp t&aacute;c với họ. Tuy nhi&ecirc;n, nhược điểm l&agrave; sẽ chịu gi&aacute; cao hơn so với gi&aacute; gốc nhưng bạn lại được chọn h&agrave;ng kỹ hơn, tr&aacute;nh việc tồn kho, h&agrave;ng lỗi.</li>\r\n	<li>Nhập h&agrave;ng từ nước ngo&agrave;i: Nước ngo&agrave;i như Trung Quốc, H&agrave;n Quốc, Th&aacute;i Lan&hellip;c&oacute; rất nhiều mặt h&agrave;ng hay ho, độc đ&aacute;o gi&uacute;p bạn thoải m&aacute;i lựa chọn. Lấy h&agrave;ng từ nước ngo&agrave;i dễ chiếm được cảm t&igrave;nh của người ti&ecirc;u d&ugrave;ng bởi kh&ocirc;ng dễ bị đụng h&agrave;ng. Hơn nữa, b&aacute;n h&agrave;ng phụ kiện điện thoại nhập từ nước ngo&agrave;i c&oacute; lợi nhuận si&ecirc;u lớn.</li>\r\n</ul>\r\n\r\n<h2>5. Kinh doanh phụ kiện điện thoại theo h&igrave;nh thức n&agrave;o?</h2>\r\n\r\n<p>Việc x&aacute;c định h&igrave;nh thức kinh doanh phụ thuộc v&agrave;o yếu tố vốn cũng như kinh nghiệm bu&ocirc;n b&aacute;n, quản l&yacute; của bản th&acirc;n.</p>\r\n\r\n<p>Nếu c&oacute; nhiều vốn, bạn c&oacute; thể vừa mở shop vừa b&aacute;n online để tăng th&ecirc;m kh&aacute;ch h&agrave;ng tr&ecirc;n to&agrave;n quốc. Với h&igrave;nh thức n&agrave;y, bạn cần phải c&oacute; khả năng quản l&yacute; cửa h&agrave;ng, c&oacute; đầu &oacute;c kinh doanh tốt để th&uacute;c đẩy hệ thống ph&aacute;t triển.</p>\r\n\r\n<p>Nếu c&oacute; &iacute;t vốn, bạn c&oacute; thể b&aacute;n h&agrave;ng online trước. Hiện nay c&oacute; rất nhiều người b&aacute;n h&agrave;ng online th&ocirc;ng qua Facebook hoặc Zalo. Chỉ cần thu&ecirc; chạy quảng c&aacute;o l&agrave; bạn đ&atilde; c&oacute; thể tăng rất nhiều lợi nhuận rồi. Ngo&agrave;i ra, bạn c&oacute; thể đăng b&aacute;n tr&ecirc;n c&aacute;c mạng thương mại điện tử như: shopee, chotot, sendo, tiki, lazada,&hellip; để t&igrave;m kiếm th&ecirc;m kh&aacute;ch h&agrave;ng tiềm năng cho shop.</p>\r\n\r\n<p><a href=\"https://www.sapo.vn/thiet-ke-website-ban-hang.html?utm_campaign=cpn:blog_ref-plm:&amp;utm_source=blog&amp;utm_medium=ref_box_web&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=blog_ref_web\" rel=\"noopener noreferrer\" target=\"_blank\"><img alt=\"\" src=\"https://www.sapo.vn/blog/thiet-ke-website-ban-hang-new.gif\" style=\"height:600px; width:800px\" /></a></p>\r\n\r\n<p><a href=\"https://www.sapo.vn/thiet-ke-website-ban-hang.html?utm_campaign=cpn:blog_ref-plm:&amp;utm_source=blog&amp;utm_medium=ref_box_web&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=blog_ref_web\" rel=\"noopener noreferrer\" target=\"_blank\">B&aacute;n h&agrave;ng online l&agrave; phải c&oacute; website</a></p>\r\n\r\n<p>Bạn đ&atilde; c&oacute; website b&aacute;n h&agrave;ng chưa? Quảng b&aacute; sản phẩm cho 90 triệu người Việt, th&uacute;c đẩy gấp đ&ocirc;i doanh số? Tạo ngay&nbsp;<a href=\"https://www.sapo.vn/thiet-ke-website-ban-hang.html?utm_campaign=cpn:blog_ref-plm:&amp;utm_source=blog&amp;utm_medium=ref_box_web&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=blog_ref_web\" rel=\"noopener\" target=\"_blank\">website b&aacute;n h&agrave;ng chuy&ecirc;n nghiệp</a>&nbsp;v&agrave; bắt đầu th&ocirc;i n&agrave;o!</p>\r\n\r\n<h2>6. Quản l&yacute; cửa h&agrave;ng kinh doanh phụ kiện điện thoại</h2>\r\n\r\n<p>Khi đ&atilde; đi v&agrave;o hoạt động, shop của bạn cần phải c&oacute; phần mềm quản l&yacute; để kiểm k&ecirc; h&agrave;ng h&oacute;a cũng như doanh thu của cửa h&agrave;ng. Nếu phụ kiện của bạn qu&aacute; nhiều, bạn sẽ chẳng thể n&agrave;o đếm v&agrave; ghi ch&eacute;p sổ s&aacute;ch ch&iacute;nh x&aacute;c được. Sử dụng&nbsp;<a href=\"https://www.sapo.vn/bang-gia.html?utm_campaign=cpn:pr_action-plm:article&amp;utm_source=blog_sapo_pr&amp;utm_medium=internal&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=baiviet_pr\" target=\"_blank\">phần mềm b&aacute;n h&agrave;ng</a>&nbsp;vừa tiện lợi cho việc xuất &ndash; nhập lại dễ d&agrave;ng kiểm k&ecirc; được l&atilde;i lỗ của shop. Hơn thế nữa, bạn c&oacute; thể kiểm tra mọi hoạt động kinh doanh b&aacute;n h&agrave;ng ở bất cứ đ&acirc;u với chiếc điện thoại của m&igrave;nh m&agrave; kh&ocirc;ng cần phải c&oacute; mặt tại cửa h&agrave;ng.</p>\r\n\r\n<p>Xem th&ecirc;m:&nbsp;<a href=\"https://shop.sapo.vn/may-in-hoa-don-may-in-bill?utm_campaign=cpn:ref_random-plm:article&amp;utm_source=blog&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=blog_ref_inbill_random\" rel=\"nofollow\" target=\"_blank\">M&aacute;y in h&oacute;a đơn</a>&nbsp;cho cửa h&agrave;ng điện thoại</p>\r\n\r\n<p>Bạn đ&atilde; biết được kinh doanh phụ kiện điện thoại cần những g&igrave; rồi phải kh&ocirc;ng? Sapo ch&uacute;c c&aacute;c bạn th&agrave;nh c&ocirc;ng với &yacute; tưởng tuyệt vời n&agrave;y nh&eacute;.</p>', '2020-08-17 10:18:19', 0, 0, '2020-08-17 10:36:58'),
(4, 'Kinh nghiệm mở cửa hàng kinh doanh sim thẻ điện thoại', 'kinh-nghiem-mo-cua-hang-kinh-doanh-sim-the-dien-thoai', 1, 1, 2, 0, 'Xã hội ngày càng phát triền, nhu cầu con người ngày càng tăng ai cũng sở hữu cho mình ít nhất 1 ,2 sim điện thoại. Nhu cầu về sim thẻ điện thoại không lúc nào là lỗi thời, mà nhu cầu ngày càng tăng, kinh doanh mặt hàng này chỉ cần bỏ một số vốn ít lãi nhiều.', '2020-08-17__sim-the.jpg', '<h2 style=\"text-align:justify\">1. Địa điểm, nguồn h&agrave;ng</h2>\r\n\r\n<p style=\"text-align:justify\">Cửa h&agrave;ng th&igrave; kh&ocirc;ng cần lớn, đầu tư b&aacute;n kh&ocirc;ng phải bu&ocirc;n th&igrave; bạn chỉ cần bỏ ra 20 đến 30 triệu, d&ugrave;ng sim đa năng để kick hoạt sim cho kh&aacute;ch h&agrave;ng mỗi sim k&iacute;ck hoạt th&agrave;nh c&ocirc;ng<br />\r\nbạn đựoc 13 đến 15ngh&igrave;n t&ugrave;y theo th&aacute;ng v&iacute; dụ : Sim mới nguy&ecirc;n bộ c&oacute; gi&aacute; 65N, bạn d&ugrave;ng sim đa năng k&iacute;ck hoạt th&igrave; bạn đựoc 15ngh&igrave;n . Ở Việt Nam tỉ lệ người d&ugrave;ng sim thẻ viettel kh&aacute; cao chiếm khoảng 52.2% thị phần. Ch&iacute;nh v&igrave; thế nếu c&oacute; vốn lớn bạn c&oacute; thể nhận l&agrave;m đại l&yacute; sim thẻ Viettel cho nh&agrave; mạng. Chắc chắn đ&acirc;y sẽ l&agrave; h&igrave;nh thức kinh doanh rất b&eacute;o bở.<br />\r\nNếu l&agrave; mở của h&agrave;ng b&aacute;n b&igrave;nh thường th&igrave;, chỗ đ&ocirc;ng ngư&ograve;i l&agrave; được, hoặc chỗ dể lọt v&agrave;o tầm mắt của những người qua lại l&agrave; một lợi thế.</p>\r\n\r\n<p style=\"text-align:justify\">Nguồn h&agrave;ng th&igrave; bạn n&ecirc;n chọn những đại l&yacute; lớn để c&oacute; chiết khấu cao như đại l&yacute; sim thẻ Viettel&hellip;. C&oacute; thể nhờ người quen giới thiệu hoặc c&aacute;c trang mạng internet.</p>\r\n\r\n<p style=\"text-align:justify\">Xem th&ecirc;m:&nbsp;<a href=\"https://shop.sapo.vn/may-in-hoa-don-may-in-bill?utm_campaign=cpn:ref_random-plm:article&amp;utm_source=blog&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=blog_ref_inbill_random\" rel=\"nofollow\" target=\"_blank\">M&aacute;y in h&oacute;a đơn</a>&nbsp;cho cửa h&agrave;ng điện thoại</p>\r\n\r\n<h2 style=\"text-align:justify\">2. Sản phẩm kinh doanh</h2>\r\n\r\n<p style=\"text-align:justify\">C&aacute;ch b&aacute;n sim hiểu quả l&agrave; phải t&igrave;m trước nhu cầu của kh&aacute;ch h&agrave;ng xem họ đang cần những loại sim g&igrave;, từ đ&oacute; nhập về. Hiện tại, tr&ecirc;n thị trường c&oacute; 3 loại sim ch&iacute;nh: sim r&aacute;c, sim số đẹp, sim sinh vi&ecirc;n. Đặc biệt, d&acirc;n kinh doanh rất k&eacute;n chọn trong việc chọn số điện thoại cho ri&ecirc;ng m&igrave;nh, ho coi đ&oacute; như l&agrave; một thứ để thể hiện đẳng cấp cũng l&agrave; một thứ mang lại sự may mắn trong l&agrave;m ăn của m&igrave;nh, những số họ chọn thường l&agrave; những số dễ nhớ, tứ qu&yacute;, hoặc đu&ocirc;i số ph&aacute;t lộc với những số n&agrave;y rất nhiều người muốn c&oacute; được n&ecirc;n độn gi&aacute; l&ecirc;n rất cao nếu sim b&igrave;nh thường c&oacute; gi&aacute; 50-100k th&igrave; sim được gọi l&agrave; số đẹp n&agrave;y c&oacute; gi&aacute; gấp 3, 4 lần thậm tr&iacute; c&ograve;n hơn.</p>\r\n\r\n<p style=\"text-align:justify\">Sim sinh vi&ecirc;n được &aacute;p dụng cho c&aacute;c bạn kh&ocirc;ng l&agrave; sinh vi&ecirc;n nhưng vẫn muốn được hưởng những ưu đ&atilde;i từ c&aacute;c nh&agrave; mạng vinaphone, viettel, mobiphone&hellip; mỗi th&aacute;ng được cộng 25k v&agrave;o t&agrave;i khoản nội mạng, 30000kb truy cập mang v&agrave; 2500k dể c&oacute; 200d/1p,200tin nhắn v&agrave; rất nhiều những ưu đ&atilde;i đặc biệt kh&aacute;c d&agrave;nh cho c&aacute;c sim n&agrave;y. Cước gọi v&agrave; nhắn tin cũng rẻ hơn rất nhiều so với những sim thường n&ecirc;n c&oacute; rất nhiều người ch&uacute; &yacute; v&agrave; muốn mua loại sim n&agrave;y.</p>\r\n\r\n<p style=\"text-align:justify\">Cũng c&oacute; những người muốn giữ một số cố định v&agrave; ch&iacute;nh chủ sim m&igrave;nh đang d&ugrave;ng từ trước đến nay th&igrave; bạn n&ecirc;n l&agrave;m th&ecirc;m dịch vụ&nbsp;chuyển sim thường sang sim sinh vi&ecirc;n. Chỉ cần chụp ảnh cmt v&agrave; 5 số thường li&ecirc;n lạc l&agrave; bạn c&oacute; thể sử dụng ngay một sim sinh vi&ecirc;n nhiều ưu đ&atilde;i</p>\r\n\r\n<h2 style=\"text-align:justify\">3. Một v&agrave;i m&aacute;nh kh&oacute;e khi kinh doanh mặt h&agrave;ng n&agrave;y</h2>\r\n\r\n<p style=\"text-align:justify\">Tại một điểm b&aacute;n sim thẻ ở 45 Đội Cấn, H&agrave; Nội, c&aacute;c quảng c&aacute;o về khuyến mại thẻ c&agrave;o &ldquo;khủng&rdquo; cũng khiến cho người qua đường phải giật m&igrave;nh: Nạp 50.000 đồng được 150.000 đồng; Nạp 100.000 đồng được 250.000 đồng; Nạp 130.000 đồng được 400.000 đồng trong t&agrave;i khoản. Đặc biệt khi nạp 300.000 đồng sẽ c&oacute; 1 triệu đồng trong t&agrave;i khoản.</p>\r\n\r\n<p style=\"text-align:justify\">Theo t&igrave;m hiểu của&nbsp;VnExpress.net, để c&oacute; thể đưa lại lợi &iacute;ch về tặng t&agrave;i khoản đối với thẻ nạp tiền cao hơn cả mức của nh&agrave; cung cấp cho ph&eacute;p, chủ điểm b&aacute;n sim thẻ đ&atilde; chấp nhận phụ th&ecirc;m tiền v&agrave;o t&agrave;i khoản tiền của kh&aacute;ch h&agrave;ng. Sau đ&oacute;, &ldquo;bắn&rdquo; số tiền từ t&agrave;i khoản ch&iacute;nh của kh&aacute;ch h&agrave;ng v&agrave;o một số m&aacute;y kh&aacute;c của m&igrave;nh để chia tiền khuyến mại. N&oacute;i c&aacute;ch kh&aacute;c, chủ điểm b&aacute;n sim thẻ đ&atilde; chấp nhận phần thiệt trong khuyến mại để b&aacute;n được nhiều hơn cho c&aacute;c kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p style=\"text-align:justify\">Chẳng hạn, nếu kh&aacute;ch h&agrave;ng nạp 50.000 đồng th&igrave; chủ cửa h&agrave;ng sẽ b&ugrave; cho kh&aacute;ch 50.000 đồng nữa để nạp thẻ c&oacute; mệnh gi&aacute; 100.000 đồng. Sau khi nạp thẻ, số tiền trong t&agrave;i khoản của kh&aacute;ch h&agrave;ng sẽ c&oacute; 230.000 đồng (gồm cả t&agrave;i khoản ch&iacute;nh 100.000 đồng v&agrave; t&agrave;i khoản phụ 130.000 đồng).</p>\r\n\r\n<p style=\"text-align:justify\">Tiếp đ&oacute;, chủ cửa h&agrave;ng sẽ bắn 95.000 đồng từ t&agrave;i khoản gốc của kh&aacute;ch h&agrave;ng v&agrave;o một số điện thoại của m&igrave;nh để hưởng mức khuyến m&atilde;i 100% gi&aacute; trị thẻ nạp. Nghĩa l&agrave; họ chỉ bỏ ra 50.000 đồng để thu về 95.000 đồng khuyến m&atilde;i v&agrave; 5.000 đồng ph&iacute; chuyển tiền. C&ograve;n kh&aacute;ch h&agrave;ng th&igrave; nhận được số tiền 130.000 đồng trong t&agrave;i khoản khuyến mại. C&aacute;ch l&agrave;m tương tự cũng &aacute;p dụng với c&aacute;c thẻ c&agrave;o c&oacute; mệnh gi&aacute; lớn hơn.</p>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i việc khuyến mại thẻ c&agrave;o &ldquo;khủng&rdquo;, c&aacute;c cửa h&agrave;ng, đại l&yacute; thẻ sim c&ograve;n chạy đua b&aacute;n ph&aacute; gi&aacute; thẻ sim để c&acirc;u kh&aacute;ch.&nbsp;Một chiếc sim mệnh gi&aacute; 65.000 đồng (bộ nạp sẵn 50.000 đồng) nhưng c&aacute;c điểm b&aacute;n sim thẻ thường chỉ b&aacute;n với gi&aacute; cao nhất 45.000 &ndash; 50.000 đồng v&agrave; c&oacute; sẵn từ 145.000 -147.000 đồng trong t&agrave;i khoản, t&ugrave;y loại sim.</p>\r\n\r\n<p style=\"text-align:justify\">Ph&oacute; gi&aacute;m đốc C&ocirc;ng ty Viettel Telecom &ndash; B&ugrave;i Quang Tuyến cho hay hiện tượng tr&ecirc;n bắt đầu rộ l&ecirc;n từ c&aacute;ch đ&acirc;y v&agrave;i th&aacute;ng khi c&aacute;c nh&agrave; cung cấp dịch vụ tăng cường chương tr&igrave;nh khuyến m&atilde;i.</p>\r\n\r\n<p style=\"text-align:justify\">&Ocirc;ng Tuyến cho rằng, &ldquo;chi&ecirc;u thổi gi&aacute;&rdquo; của c&aacute;c cửa h&agrave;ng, đại l&yacute; tr&ecirc;n theo &ocirc;ng Tuyến kh&ocirc;ng phạm luật m&agrave; chỉ l&agrave; h&igrave;nh thức &ldquo;l&aacute;ch c&ocirc;ng nghệ&rdquo;. Về cơ bản, kh&aacute;ch h&agrave;ng l&agrave; đối tượng được lợi khi số tiền bắn v&agrave;o t&agrave;i khoản nhiều hơn v&agrave; đại l&yacute; cửa h&agrave;ng cũng kiếm lợi ch&uacute;t &iacute;t, chỉ c&oacute; nh&agrave; cung cấp dịch vụ l&agrave; thiệt th&ograve;i.</p>\r\n\r\n<p style=\"text-align:justify\">Theo nguồn&nbsp;tin&nbsp;từ c&aacute;c mạng di động kh&aacute;c, sự cạnh tranh qu&aacute; lớn đ&atilde; buộc c&aacute;c đại l&yacute; sim thẻ phải chấp nhận &ldquo;ăn &iacute;t&rdquo; để b&aacute;n được nhiều h&agrave;ng. Đối với một số loại sim thẻ kh&oacute; b&aacute;n như Vietnamobile, Beeline, một số đại l&yacute; thậm ch&iacute; ngưng nhập hoặc chấp nhận b&aacute;n lỗ ch&uacute;t &iacute;t để khỏi bị đọng vốn.</p>\r\n\r\n<p style=\"text-align:justify\">Giới chuy&ecirc;n gia nhận định cạnh tranh tr&ecirc;n thị trường viễn th&ocirc;ng giờ đ&acirc;y kh&ocirc;ng c&ograve;n l&agrave; c&acirc;u chuyện ri&ecirc;ng của c&aacute;c h&atilde;ng di động m&agrave; c&ograve;n l&agrave; cuộc chiến quyết liệt giữa c&aacute;c cửa h&agrave;ng đại l&yacute; b&aacute;n thẻ c&agrave;o&hellip;</p>', '2020-08-17 10:22:46', 0, 1, '2020-08-17 10:36:59'),
(5, 'Kinh doanh phụ kiện điện thoại, ý tưởng kinh doanh nhỏ hốt bạc', 'kinh-doanh-phu-kien-dien-thoai-y-tuong-kinh-doanh-nho-hot-bac', 1, 1, 2, 0, 'Kinh doanh phụ kiện điện thoại lời gấp 3 – 4 lần, đang trở thành nghề hot được nhiều người quan tâm. Bạn hoàn toàn có thể bắt đầu ý tưởng kinh doanh phụ kiện điện thoại để khởi nghiệp kinh doanh cho riêng mình', '2020-08-17__kinh-doanh-sim-the.jpg', '<p style=\"text-align:justify\">Kinh doanh phụ kiện điện thoại tưởng chừng kh&oacute; kiếm lời nhưng thực chất lại cực si&ecirc;u lợi nhuận.</p>\r\n\r\n<h2 style=\"text-align:justify\">V&igrave; sao n&ecirc;n kinh doanh phụ kiện điện thoại?</h2>\r\n\r\n<p style=\"text-align:justify\">Trước ti&ecirc;n bạn cần trả lời những c&acirc;u hỏi dưới đ&acirc;y để x&aacute;c định xem c&oacute; n&ecirc;n kinh doanh phụ kiện điện thoại kh&ocirc;ng nh&eacute;!</p>\r\n\r\n<p style=\"text-align:justify\">Thực tế nhu cầu sử dụng phụ kiện điện thoại hiện nay rất cao. Bất cứ ai cũng đều sở hữu cho m&igrave;nh một sản phẩm điện thoại th&ocirc;ng minh, m&agrave; những phụ kiện điện thoại đều c&oacute; tuổi thọ ngắn hạn. Trong qu&aacute; tr&igrave;nh sử dụng rất dễ bị hỏng, xước, vỡ, phai m&agrave;u, đứt d&acirc;y&hellip;n&ecirc;n họ sẽ lại t&igrave;m mua mới. Thấy nhu cầu của người d&ugrave;ng tương đối cao n&ecirc;n đ&acirc;y sẽ l&agrave; thị trường b&eacute;o bở cho bạn thử nghiệm đấy.</p>\r\n\r\n<p style=\"text-align:justify\">Bất cứ ng&agrave;nh nghề n&agrave;o cũng vậy, vốn l&agrave; điều kiện ti&ecirc;n quyết gi&uacute;p bạn khởi động mọi chiến dịch của m&igrave;nh. Vậy kinh doanh phụ kiện điện thoại cần bao nhi&ecirc;u vốn? C&acirc;u trả lời kh&ocirc;ng thể ch&iacute;nh x&aacute;c được. Bởi số vốn bỏ ra nhiều hay &iacute;t do m&ocirc; h&igrave;nh kinh doanh m&agrave; bạn chọn lựa. Nếu kinh doanh nhỏ, bạn chỉ mất tiền nhập h&agrave;ng, mặt bằng, bảng hiệu, m&oacute;c, lưới treo phụ kiện,&hellip;C&ograve;n nếu kinh doanh to hơn, ngo&agrave;i những chi ph&iacute; tr&ecirc;n bạn c&ograve;n mất th&ecirc;m chi ph&iacute; thu&ecirc; nh&acirc;n vi&ecirc;n, tiền đặt cọc địa điểm, tiền mua phần mềm quản l&yacute; b&aacute;n h&agrave;ng,&hellip;</p>\r\n\r\n<p style=\"text-align:justify\">Điều bạn cần l&agrave;m l&agrave; lập kế hoạch kinh doanh với nguồn vốn sẵn c&oacute; của m&igrave;nh để kh&ocirc;ng gặp phải những rắc rối trước mắt.</p>\r\n\r\n<h2 style=\"text-align:justify\">&Yacute; tưởng kinh doanh phụ kiện điện thoại bạn c&oacute; thể &aacute;p dụng</h2>\r\n\r\n<p style=\"text-align:justify\">Với loại sản phẩm n&agrave;y, bạn c&oacute; thể &aacute;p dụng cả h&igrave;nh thức kinh doanh mở shop v&agrave; kinh doanh phụ kiện điện thoại online. D&ugrave; bất cứ h&igrave;nh thức n&agrave;o th&igrave; bạn đều hoạt động dễ d&agrave;ng, đặc biệt l&agrave; online bởi bạn sẽ c&oacute; lượng kh&aacute;ch h&agrave;ng tr&ecirc;n to&agrave;n quốc chứ kh&ocirc;ng chỉ một khu vực quanh địa điểm kinh doanh. Khi đi v&agrave;o hoạt động, cả mở shop v&agrave; online th&igrave; bạn cần phải c&oacute; những chiến lược PR, quảng b&aacute; ph&ugrave; hợp để thu h&uacute;t l&ocirc;i k&eacute;o kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<h2 style=\"text-align:justify\">Lợi thế khi chọn &yacute; tưởng kinh doanh phụ kiện điện thoại</h2>\r\n\r\n<h3 style=\"text-align:justify\">Vốn đầu tư kinh doanh thấp</h3>\r\n\r\n<p style=\"text-align:justify\">Như b&ecirc;n tr&ecirc;n đ&atilde; n&oacute;i, vốn kinh doanh phụ kiện điện thoại kh&ocirc;ng y&ecirc;u cầu qu&aacute; cao. Tuy nhi&ecirc;n, nếu m&agrave; b&aacute;n được th&igrave; sẽ thu được lợi nhuận rất lớn. C&aacute;c sản phẩm phụ kiện điện thoại như ốp, d&aacute;n k&iacute;nh cường lực, tai nghe, pin, sạc, d&acirc;y c&aacute;p,&hellip;tuy c&oacute; gi&aacute; rất nhỏ nhưng v&igrave; sức mua lớn n&ecirc;n hiệu quả kinh doanh l&agrave; rất cao.</p>\r\n\r\n<p style=\"text-align:justify\">Hơn thế nữa, hiện nay ai cũng c&oacute; một chiếc điện thoại smartphone trong tay. Gi&aacute; trị của smartphone kh&ocirc;ng hề nhỏ n&ecirc;n việc bảo quản, giữ g&igrave;n l&agrave; điều v&ocirc; c&ugrave;ng quan trọng. Bất cứ ai mua điện thoại đều cần phải c&oacute; vỏ ốp, d&aacute;n k&iacute;nh cường lực, tai nghe, sạc, pin dự ph&ograve;ng&hellip;để đề ph&ograve;ng trong trường hợp cần d&ugrave;ng. V&igrave; v&agrave;i th&aacute;ng l&agrave; người d&ugrave;ng lại thay đổi phụ kiện một lần n&ecirc;n nhu cầu mua sắm rất cao, vốn xoay v&ograve;ng dễ.</p>\r\n\r\n<h3 style=\"text-align:justify\">Nguồn h&agrave;ng phụ kiện điện thoại dồi d&agrave;o</h3>\r\n\r\n<p style=\"text-align:justify\">Điều quan trọng khi bạn kinh doanh l&agrave; phải t&igrave;m được nguồn h&agrave;ng phụ kiện điện thoại chất lượng, gi&aacute; cả hợp l&yacute;, l&agrave;m ăn l&acirc;u d&agrave;i, uy t&iacute;n. Nguồn h&agrave;ng ch&iacute;nh l&agrave; yếu tố quan trọng gi&uacute;p bạn c&oacute; thể cạnh tranh được với c&aacute;c đối thủ tr&ecirc;n thị trường.</p>\r\n\r\n<p style=\"text-align:justify\">C&oacute; kh&aacute; nhiều hướng đi cho c&aacute;c bạn lấy h&agrave;ng, c&oacute; thể tới tận cơ sở sản xuất phụ kiện điện thoại hoặc trở th&agrave;nh chi nh&aacute;nh, đại l&yacute; lấy sỉ từ c&aacute;c cửa h&agrave;ng đổ bu&ocirc;n lớn.</p>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i ra, những m&oacute;n phụ kiện được nhập khẩu từ nước ngo&agrave;i về kh&aacute; độc đ&aacute;o v&agrave; đẹp mắt. H&igrave;nh thức, chất lượng đều kh&ocirc;ng c&oacute; g&igrave; phải b&agrave;n c&atilde;i. Nếu quen biết c&aacute;c c&ocirc;ng ty nhập h&agrave;ng th&igrave; c&oacute; thể th&ocirc;ng qua họ để lấy h&agrave;ng. Quan trọng nhất vẫn l&agrave; t&igrave;m được nguồn cung cấp đẹp, rẻ, chất lượng th&igrave; bạn sẽ c&oacute; nhiều l&atilde;i hơn.</p>\r\n\r\n<p style=\"text-align:justify\">T&ugrave;y v&agrave;o từng độ tuổi của kh&aacute;ch h&agrave;ng m&agrave; chọn loại phụ kiện kh&aacute;c nhau. Nếu l&agrave; đối tượng học sinh, sinh vi&ecirc;n th&igrave; c&aacute;c loại phụ kiện độc đ&aacute;o, ngộ nghĩnh dễ thương (cho nữ), c&aacute; t&iacute;nh (cho nam) sẽ gi&uacute;p bạn kiếm được nhiều kh&aacute;ch h&agrave;ng hơn. C&ograve;n mặt h&agrave;ng d&agrave;nh cho đối tượng tr&ecirc;n 30 tuổi th&igrave; c&aacute;c phụ kiện sang trọng, đẹp mắt v&agrave; bền sẽ được họ ch&uacute; &yacute;.</p>\r\n\r\n<h3 style=\"text-align:justify\">Ph&ugrave; hợp thị hiếu người d&ugrave;ng</h3>\r\n\r\n<p style=\"text-align:justify\">C&aacute;c loại h&agrave;ng phụ kiện điện thoại n&agrave;y c&oacute; ưu điểm l&agrave; đa dạng, phong ph&uacute; c&oacute; thể đ&aacute;p ứng đầy đủ nhu cầu của nhiều đối tượng kh&aacute;ch h&agrave;ng. C&aacute;c phụ kiện ốp lưng đầy m&agrave;u sắc, c&oacute; h&igrave;nh ngộ nghĩnh, đ&iacute;nh đ&aacute;, hạt cườm,&hellip;.sẽ tăng th&ecirc;m vẻ đẹp tinh tế cho chiếc điện thoại của bạn. Chắc chắn chả c&oacute; kh&aacute;ch h&agrave;ng n&agrave;o từ chối được việc l&agrave;m mới cho dế y&ecirc;u của họ.</p>\r\n\r\n<p style=\"text-align:justify\">Nếu c&oacute; h&agrave;ng h&oacute;a tồn đọng, h&atilde;y đưa ra những chiến dịch giảm gi&aacute;, thanh l&yacute; để xoay v&ograve;ng vốn v&agrave; nhập th&ecirc;m h&agrave;ng mới về. H&atilde;y kết hợp nhuần nhuyễn chiến dịch b&aacute;n h&agrave;ng tại shop với online để n&acirc;ng cao doanh thu. Ch&uacute;c bạn kinh doanh phụ kiện điện thoại online th&agrave;nh c&ocirc;ng.</p>', '2020-08-17 10:23:49', 1, 0, '2020-08-17 10:37:05');
INSERT INTO `articles` (`id`, `a_name`, `a_slug`, `a_hot`, `a_active`, `a_menu_id`, `a_view`, `a_description`, `a_avatar`, `a_content`, `created_at`, `a_position_2`, `a_position_1`, `updated_at`) VALUES
(6, 'Tìm nguồn hàng phụ kiện điện thoại ở đâu?', 'tim-nguon-hang-phu-kien-dien-thoai-o-dau', 1, 1, 2, 0, 'Tại bài viết trước về kinh doanh và quản lý bán hàng online phụ kiện chúng tôi đã từng đưa ra những lợi thế và thách thức của ý tưởng này, trong đó có một thuận lợi là nguồn hàng phong phú. Chắc hẳn nhiều bạn đang muốn biết chi tiết hơn về những nguồn hàng này, nếu vậy thì hãy tham khảo thêm ngay sau đây.', '2020-08-17__nguon-hang-phu-kien-dien-thoai-2.jpg', '<ol>\r\n	<li>\r\n	<h2>Chợ đầu mối</h2>\r\n	</li>\r\n</ol>\r\n\r\n<p>C&aacute;c chợ đầu mối l&agrave; một trong những địa chỉ quen thuộc của d&acirc;n bu&ocirc;n b&aacute;n, tại đ&acirc;y c&oacute; h&agrave;ng trăm nguồn h&agrave;ng sỉ với gi&aacute; cả cực k&igrave; cạnh tranh, số lượng sản phẩm rất nhiều v&agrave; phong ph&uacute;. Đối với c&aacute;c mặt h&agrave;ng phụ kiện điện thoại cũng vậy, bạn sẽ dễ d&agrave;ng t&igrave;m thấy c&aacute;c loại ốp lưng, c&aacute;p sạc, pin dự ph&ograve;ng,&hellip; như &yacute; khi dạo một v&ograve;ng chợ. Tại S&agrave;i G&ograve;n c&oacute; c&aacute;c chợ nổi tiếng như Kim Bi&ecirc;n, An Đ&ocirc;ng, B&igrave;nh T&acirc;y, c&ograve;n ở H&agrave; Nội th&igrave; c&oacute; chợ Đồng Xu&acirc;n, chợ trời Thịnh y&ecirc;n,&hellip;</p>\r\n\r\n<p>B&ecirc;n cạnh những điểm về gi&aacute; v&agrave; sự phong ph&uacute; của h&agrave;ng h&oacute;a th&igrave; nguồn sỉ của chợ đầu mối cũng tồn tại một số nhược điểm, như l&agrave; chất lượng kh&ocirc;ng đồng đều, phải tinh mắt v&agrave; c&oacute; kinh nghiệm lựa chọn, cần biết c&aacute;ch trả gi&aacute;,&hellip;. V&igrave; vậy bạn n&ecirc;n đi c&ugrave;ng những người đ&atilde; c&oacute; th&acirc;m ni&ecirc;n trong nghề trong lần đầu lấy h&agrave;ng để nhập được mối tốt nhất.</p>\r\n\r\n<ol start=\"2\">\r\n	<li>\r\n	<h2>Cửa h&agrave;ng phụ kiện điện thoại lớn</h2>\r\n	</li>\r\n</ol>\r\n\r\n<p>Nếu kh&ocirc;ng c&oacute; điều kiện đi chợ đầu mối đ&aacute;nh h&agrave;ng th&igrave; bạn c&oacute; thể tới những cửa h&agrave;ng lớn chuy&ecirc;n kinh doanh phụ kiện điện thoại để đặt vấn đề, thương thảo về việc nhập sỉ số lượng lớn. Khi đến đ&acirc;y bạn sẽ được tham khảo c&aacute;ch thức họ kinh doanh, đ&aacute;nh gi&aacute; sản phẩm n&agrave;o đang b&aacute;n chạy, học hỏi những kiến thức về chăm s&oacute;c kh&aacute;ch h&agrave;ng,&hellip; sẽ gi&uacute;p &iacute;ch rất nhiều cho việc mở cửa h&agrave;ng phụ kiện điện thoại của bạn. Tuy vậy, bạn phải chấp nhận rằng gi&aacute; sỉ ở đ&acirc;y sẽ cao hơn một ch&uacute;t v&igrave; đ&atilde; qua trung gian.</p>\r\n\r\n<p><img alt=\"nguon-hang-phu-kien-dien-thoai-1\" src=\"https://i0.wp.com/www.sapo.vn/blog/wp-content/uploads/2016/10/nguon-hang-phu-kien-dien-thoai-1.jpg?resize=600%2C465&amp;quality=100&amp;strip=all&amp;ssl=1\" style=\"height:465px; width:600px\" /></p>\r\n\r\n<ol start=\"3\">\r\n	<li>\r\n	<h2>Nhập h&agrave;ng từ Quảng Ch&acirc;u &ndash; Trung Quốc</h2>\r\n	</li>\r\n</ol>\r\n\r\n<p>Thực tế đa phần h&agrave;ng phụ kiện điện thoại ở nước ta hiện nay đều c&oacute; xuất xứ từ Trung Quốc, v&igrave; vậy d&ugrave; bạn lấy sỉ từ chợ đầu mối th&igrave; vẫn kh&ocirc;ng phải l&agrave; lấy tận gốc. Để c&oacute; gi&aacute; bu&ocirc;n thấp nhất, đồng thời lựa được nhiều loại sản phẩm hơn, một số người sẵn s&agrave;ng bỏ c&ocirc;ng, bỏ sức, bỏ tiền sang tận Trung Quốc, tới thi&ecirc;n đường mua sắm tại Quảng Ch&acirc;u để đ&aacute;nh h&agrave;ng.</p>\r\n\r\n<p>Tại Quảng Ch&acirc;u c&oacute; một số khu chợ chuy&ecirc;n về điện thoại, phụ kiện như Photography Electronics City, Da Sha Tou, Thi&ecirc;n H&agrave;,&hellip; Bạn c&oacute; thể tham khảo th&ecirc;m b&agrave;i viết&nbsp;<a href=\"https://www.sapo.vn/blog/kinh-nghiem-lay-hang-quang-chau-cho-nguoi-moi/\">Kinh nghiệm lấy h&agrave;ng Quảng Ch&acirc;u cho người mới</a>&nbsp;để được hướng dẫn sang nước bạn nhập sỉ.</p>\r\n\r\n<ol start=\"4\">\r\n	<li>\r\n	<h2>Đặt h&agrave;ng phụ kiện điện thoại tr&ecirc;n website</h2>\r\n	</li>\r\n</ol>\r\n\r\n<p>Ngo&agrave;i những nguồn h&agrave;ng trực tiếp như tr&ecirc;n th&igrave; bạn c&ograve;n c&oacute; thể t&igrave;m đến một nguồn h&agrave;ng kh&aacute;c đơn giản hơn, kh&ocirc;ng mất thời gian t&igrave;m kiếm, kh&ocirc;ng mất c&ocirc;ng di chuyển, đ&oacute; l&agrave; những website chuy&ecirc;n đổ sỉ đồ phụ kiện điện thoại. Bạn chỉ cần l&ecirc;n mạng, t&igrave;m từ kh&oacute;a &ldquo;phụ kiện điện thoại&rdquo;, ngay lập tức sẽ c&oacute; h&agrave;ng trăm kết quả trả về, một số trang web phổ biến l&agrave; phukiengiabuon.vn, linhphukiengiagoc.vn, thegioiphukien.vn,&hellip;.</p>\r\n\r\n<p>Những website n&agrave;y c&oacute; đầy đủ c&aacute;c loại phụ kiện cho nhiều d&ograve;ng điện thoại kh&aacute;c nhau, gi&aacute; cả kh&aacute; rẻ nếu mua với số lượng lớn. Một số trang c&ograve;n c&oacute; ch&iacute;nh s&aacute;ch giao h&agrave;ng với nhiều ưu đ&atilde;i d&agrave;nh cho những ai thường xuy&ecirc;n đặt sản phẩm tại đ&oacute;. Tuy nhi&ecirc;n, do mua h&agrave;ng trực tuyến kh&ocirc;ng được tiếp x&uacute;c trực tiếp với h&agrave;ng h&oacute;a n&ecirc;n kh&oacute; l&ograve;ng kiểm định được chất lượng, v&igrave; vậy lần đầu chỉ bạn n&ecirc;n nhập &iacute;t sẽ kh&ocirc;ng lo bị lừa.</p>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y l&agrave; 4 nguồn h&agrave;ng phụ kiện điện thoại phổ biến được nhiều người lựa chọn khi muốn bắt đầu kinh doanh. Hi vọng những th&ocirc;ng tin n&agrave;y sẽ hữu &iacute;ch với bạn!</p>', '2020-08-17 10:25:18', 1, 1, '2020-08-17 10:37:07'),
(7, 'Làm Giàu Từ Kinh Doanh Cửa Hàng Phụ Kiện Điện Thoại', 'lam-giau-tu-kinh-doanh-cua-hang-phu-kien-dien-thoai', 1, 1, 2, 0, 'Chúng ta đang chứng kiến cuộc đọ sức vô cùng khốc giữa các “ông lớn” di động trong nước. Số lượng người dùng smartphone và các thiết bị liên lạc thông minh ngày một gia tăng nhanh chóng. Vì thế, đây chính là cơ hội kinh doanh lý tưởng mà không nên bỏ qua ở thời điểm hiện tại.', '2020-08-17__quan-ly-san-pham-mot-cach-hieu-qua.jpg', '<p style=\"text-align:justify\">nếu bạn lựa chọn lĩnh vực&nbsp;<a href=\"https://www.uplevo.com/blog/khoi-nghiep/y-tuong-kinh-doanh-nho-tai-nha/\" rel=\"noopener noreferrer\" target=\"_blank\">kinh doanh nhỏ</a>&nbsp;với số vốn &iacute;t v&agrave; thiếu kinh nghiệm th&igrave; c&aacute;c &yacute; tưởng kinh doanh điện thoại hay m&aacute;y t&iacute;nh bảng lại l&agrave; một lựa chọn thiếu kh&ocirc;n ngoan. Thay v&agrave;o đ&oacute; bạn c&oacute; thể hướng tới một lựa chọn kh&aacute;c l&agrave;&nbsp;<strong>kinh doanh phụ kiện điện thoại</strong>. Bởi nhu cầu của mọi người đối với c&aacute;c sản phẩm để l&agrave;m đẹp cho &ldquo;ch&uacute; dế&rdquo; của m&igrave;nh ng&agrave;y nay l&agrave; kh&ocirc;ng hề nhỏ.</p>\r\n\r\n<p style=\"text-align:justify\">Nếu để n&oacute;i về 3 l&yacute; do lớn nhất giải th&iacute;ch cho c&acirc;u hỏi: V&igrave; sao n&ecirc;n&nbsp;<strong>mở cửa h&agrave;ng phụ kiện điện thoại</strong>, th&igrave; chắc chắn đ&oacute; l&agrave;:</p>\r\n\r\n<ol>\r\n	<li style=\"text-align: justify;\"><strong>Vốn &iacute;t, rủi ro thấp, l&atilde;i cao</strong></li>\r\n	<li style=\"text-align: justify;\"><strong>Nhu cầu thị trường kh&ocirc;ng bao giờ giảm</strong></li>\r\n	<li style=\"text-align: justify;\"><strong>Nguồn h&agrave;ng v&ocirc; c&ugrave;ng rộng lớn</strong></li>\r\n</ol>\r\n\r\n<p style=\"text-align:justify\">H&atilde;y theo d&otilde;i b&agrave;i viết n&agrave;y của Uplevo để t&iacute;ch luỹ th&ecirc;m cho m&igrave;nh một sự lựa chọn th&ocirc;ng minh l&agrave; L&agrave;m gi&agrave;u từ kinh doanh phụ kiện điện thoại nh&eacute;!</p>\r\n\r\n<h2 style=\"text-align:justify\"><strong>M&ocirc; h&igrave;nh kinh doanh phụ kiện điện thoại</strong></h2>\r\n\r\n<p style=\"text-align:justify\">Tuỳ theo số vốn kinh doanh v&agrave; kinh nghiệm quản l&yacute; m&agrave; bạn c&oacute; thể lựa chọn h&igrave;nh thức kinh doanh ph&ugrave; hợp. Tuy nhi&ecirc;n, bạn kh&ocirc;ng cần phải qu&aacute; đầu tư v&agrave;o gian h&agrave;ng cho ph&ocirc; trương hay ho&agrave;nh tr&aacute;ng. Chỉ cần một gian h&agrave;ng nhỏ nhỏ, vừa đủ để trưng b&agrave;y được hết c&aacute;c sản phẩm.</p>\r\n\r\n<p style=\"text-align:justify\">Ch&uacute; &yacute; một điều l&agrave; n&ecirc;n chọn địa điểm mở cửa h&agrave;ng ở những nơi c&oacute; đ&ocirc;ng người qua lại để tiếp cận kh&aacute;ch h&agrave;ng dễ d&agrave;ng hơn. Ngo&agrave;i ra, h&atilde;y x&acirc;y dựng th&ecirc;m h&igrave;nh thức kinh doanh online tr&ecirc;n Facebook hoặc c&aacute;c trang thương mại điện tử như Shopee, Lazada, Sendo,&hellip;</p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"mô hình kinh doanh phụ kiện điện thoại\" src=\"https://www.uplevo.com/blog/wp-content/uploads/2019/07/mo-hinh-kinh-doanh-phu-kien-dien-thoai.jpg\" style=\"height:512px; width:876px\" /></p>\r\n\r\n<p style=\"text-align:justify\">Với h&igrave;nh thức kinh doanh online n&agrave;y, bạn cho nhận order v&agrave; ship h&agrave;ng tận nơi sẽ c&agrave;ng thu h&uacute;t được nhiều kh&aacute;ch h&agrave;ng hơn nữa.</p>\r\n\r\n<p style=\"text-align:justify\">Chỉ cần đầu tư khoảng 10-20 triệu thu&ecirc; mặt bằng. Số tiền c&ograve;n lại d&ugrave;ng để nhập h&agrave;ng v&agrave; trang bị c&aacute;c vật liệu trang tr&iacute; cho cửa h&agrave;ng th&igrave; với&nbsp;<a href=\"https://www.uplevo.com/blog/khoi-nghiep/kinh-doanh-gi-voi-100-trieu/\" rel=\"noopener noreferrer\" target=\"_blank\">số vốn khoảng 100 triệu</a>&nbsp;l&agrave; bạn đ&atilde; ho&agrave;n to&agrave;n c&oacute; thể chi trả mọi khoản tr&ecirc;n v&agrave; c&oacute; một cửa h&agrave;ng rất tốt với số l&atilde;i l&ecirc;n đến tiền triệu mỗi ng&agrave;y.</p>\r\n\r\n<p style=\"text-align:justify\">&gt;&gt;&gt;&nbsp;<a href=\"https://www.uplevo.com/blog/khoi-nghiep/10-mo-hinh-kinh-doanh-cho-dan-khoi-nghiep/\" rel=\"noopener noreferrer\" target=\"_blank\">10 M&ocirc; H&igrave;nh Kinh Doanh D&agrave;nh Cho D&acirc;n Khởi Nghiệp</a></p>\r\n\r\n<h2 style=\"text-align:justify\"><strong>Nguồn h&agrave;ng cho c&aacute;c cửa h&agrave;ng phụ kiện điện thoại</strong></h2>\r\n\r\n<p style=\"text-align:justify\">Nếu bạn vẫn c&ograve;n băn khoăn kh&ocirc;ng biết c&oacute; n&ecirc;n kinh doanh phụ kiện điện thoại kh&ocirc;ng th&igrave; bạn cần biết rằng: Một ưu điểm nổi bật của sản phẩm phụ kiện điện thoại hiện nay đ&oacute; ch&iacute;nh l&agrave; nguồn h&agrave;ng đa dạng, phong ph&uacute; với rất rất nhiều kiểu d&aacute;ng, chủng loại kh&aacute;c nhau.</p>\r\n\r\n<p style=\"text-align:justify\">Với mặt h&agrave;ng n&agrave;y bạn cũng kh&ocirc;ng phải lo việc lỗi &ldquo;mốt&rdquo;, hết hạn sử dụng hay hao m&ograve;n. V&igrave; thế hạn chế được rất nhiều rủi ro m&agrave; c&aacute;c mặt h&agrave;ng kh&aacute;c phải đối mặt. Để c&oacute; thể lựa chọn cho m&igrave;nh một nguồn h&agrave;ng đ&aacute;p ứng được y&ecirc;u cầu về mẫu m&atilde; đẹp, gi&aacute; cạnh tranh, chất lượng tốt, nhiều cửa h&agrave;ng đ&atilde; chọn nhập h&agrave;ng bằng c&aacute;c h&igrave;nh thức như:</p>\r\n\r\n<h3 style=\"text-align:justify\"><strong>1. Lấy trực tiếp từ c&aacute;c chợ đầu mối</strong></h3>\r\n\r\n<p style=\"text-align:justify\">C&oacute; nhiều khu chợ đầu mới nổi tiếng tr&ecirc;n khắp cả nước bu&ocirc;n b&aacute;n phụ kiện điện thoại số lượng lớn gi&aacute; rẻ như: chợ Đồng Xu&acirc;n, chợ trời Thịnh Y&ecirc;n (H&agrave; Nội), chợ B&igrave;nh T&acirc;y, An Đ&ocirc;ng, Kim Bi&ecirc;n (Th&agrave;nh phố Hồ Ch&iacute; Minh) hay chợ T&acirc;n Thanh (Lạng Sơn), M&oacute;ng C&aacute;i (Quảng Ninh),&hellip;</p>\r\n\r\n<p style=\"text-align:justify\">&gt;&gt;&gt;&nbsp;<a href=\"https://www.uplevo.com/blog/kinh-doanh/huong-dan-ban-hang-tren-shopee/\" rel=\"noopener noreferrer\" target=\"_blank\">Hướng Dẫn B&aacute;n H&agrave;ng Tr&ecirc;n Shopee Hiệu Quả</a></p>\r\n\r\n<h3 style=\"text-align:justify\"><strong>2. Nhập h&agrave;ng từ c&aacute;c cửa h&agrave;ng, đại l&yacute; lớn</strong></h3>\r\n\r\n<p style=\"text-align:justify\">Đ&acirc;y được xem l&agrave; mối h&agrave;ng phổ biến v&agrave; được nhiều chủ cửa h&agrave;ng chọn lựa. C&aacute;ch n&agrave;y sẽ tiết kiệm thời gian v&agrave; chi ph&iacute; nếu kh&ocirc;ng muốn đi c&aacute;c chợ đầu mối nhập h&agrave;ng.</p>\r\n\r\n<p style=\"text-align:justify\">Gi&aacute; sỉ ở đ&acirc;y sẽ cao hơn so với một số nguồn kh&aacute;c, tuy nhi&ecirc;n chất lượng được đảm bảo v&agrave; đ&acirc;y cũng l&agrave; những mối h&agrave;ng uy t&iacute;n. Bạn cũng c&oacute; thể thương lượng để li&ecirc;n kết l&acirc;u d&agrave;i, đảm bảo cho bạn nguồn h&agrave;ng ổn định.</p>\r\n\r\n<p style=\"text-align:justify\">&gt;&gt;&gt;&nbsp;<a href=\"https://www.uplevo.com/blog/kinh-doanh/kinh-nghiem-ban-hang-tren-lazada/\" rel=\"noopener noreferrer\" target=\"_blank\">Chia Sẻ Kinh Nghiệm B&aacute;n H&agrave;ng Tr&ecirc;n Lazada Cực Hữu &Iacute;ch</a></p>\r\n\r\n<h3 style=\"text-align:justify\"><strong>3. Trực tiếp sang Trung Quốc lấy h&agrave;ng</strong></h3>\r\n\r\n<p style=\"text-align:justify\">Đ&acirc;y l&agrave; một trong số những c&aacute;ch tốt nhất m&agrave; bạn kh&ocirc;ng n&ecirc;n bỏ qua, Quảng Ch&acirc;u được xem l&agrave; thi&ecirc;n đường của phụ kiện điện thoại v&agrave; đồ c&ocirc;ng nghệ gi&aacute; rẻ. Thực chất, c&aacute;c chợ đầu mối cũng chủ yếu lấy nguồn h&agrave;ng từ Quảng Ch&acirc;u. N&ecirc;n khi bạn c&oacute; thể trực tiếp sang đấy th&igrave; sẽ nhận được gi&aacute; sỉ tốt nhất.</p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"trực tiếp sang trung quốc lấy hàng\" src=\"https://www.uplevo.com/blog/wp-content/uploads/2019/07/truc-tiep-sang-trung-quoc-lay-hang.jpg\" style=\"height:478px; width:876px\" /></p>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i ra bạn cũng c&oacute; thể nhập h&agrave;ng tr&ecirc;n c&aacute;c website v&agrave; một số h&igrave;nh thức kh&aacute;c kh&ocirc;ng phổ biến lắm. T&oacute;m lại c&oacute; rất nhiều nguồn h&agrave;ng n&ecirc;n bạn kh&ocirc;ng phải lo nghĩ nghiều.</p>\r\n\r\n<p style=\"text-align:justify\">Tuy nhi&ecirc;n h&atilde;y tham khảo nhiều mối h&agrave;ng kh&aacute;c nhau để so s&aacute;nh gi&aacute; cả v&agrave; chất lượng trước khi nhập về kinh doanh.</p>\r\n\r\n<p style=\"text-align:justify\">&gt;&gt;&gt;&nbsp;<a href=\"https://www.uplevo.com/blog/kinh-doanh/huong-dan-ban-hang-tren-sendo/\" rel=\"noopener noreferrer\" target=\"_blank\">Hướng Dẫn Kinh Doanh Tr&ecirc;n Sendo Từ A Tới Z</a></p>\r\n\r\n<h2 style=\"text-align:justify\"><strong>Kinh nghiệm b&aacute;n phụ kiện điện thoại hiệu quả</strong></h2>\r\n\r\n<p style=\"text-align:justify\">Đ&acirc;y l&agrave; một m&ocirc; h&igrave;nh kinh doanh kh&ocirc;ng qu&aacute; phức tạp nhưng để kinh doanh th&agrave;nh c&ocirc;ng th&igrave; kh&ocirc;ng c&oacute; g&igrave; l&agrave; dễ d&agrave;ng.</p>\r\n\r\n<h3 style=\"text-align:justify\">1. C&aacute;c kiến thức cơ bản về sản phẩm</h3>\r\n\r\n<p style=\"text-align:justify\">Đầu ti&ecirc;n, bạn cũng cần trang bị c&aacute;c kiến thức cần thiết về phụ kiện điện thoại. Điều n&agrave;y gi&uacute;p bạn c&oacute; thể đưa ra những tư vấn nhanh ch&oacute;ng cho kh&aacute;ch h&agrave;ng cũng như hướng dẫn cho họ khi sử dụng c&aacute;c sản phẩm. Nếu kh&aacute;ch h&agrave;ng thắc mắc m&agrave; được giải đ&aacute;p nhanh ch&oacute;ng th&igrave; chắc chắn kh&aacute;ch h&agrave;ng sẽ rất tin tưởng v&agrave; h&agrave;i l&ograve;ng với cửa h&agrave;ng.</p>\r\n\r\n<h3 style=\"text-align:justify\">2. Th&uacute;c đẩy c&aacute;c k&ecirc;nh b&aacute;n h&agrave;ng online</h3>\r\n\r\n<p style=\"text-align:justify\">Điều thứ hai, ngo&agrave;i việc đ&atilde; c&oacute; một cửa h&agrave;ng bạn n&ecirc;n th&uacute;c đẩy th&ecirc;m việc kinh doanh online. Ng&agrave;y nay, mọi người rất ưa th&iacute;ch mua sắm qua mạng vừa tiện &iacute;ch, nhanh ch&oacute;ng lại rất đẹp mắt v&agrave; thu h&uacute;t. Phụ kiện điện thoại v&iacute; dụ như: ốp lưng, sạc dự ph&ograve;ng, tai nghe,&hellip; l&agrave; những mặt h&agrave;ng qu&aacute; ưu việt để c&oacute; thể kinh doanh online.</p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"thúc đẩy các kênh bán hàng online\" src=\"https://www.uplevo.com/blog/wp-content/uploads/2019/07/thuc-day-cac-kenh-ban-hang-online.jpg\" style=\"height:450px; width:876px\" /></p>\r\n\r\n<p style=\"text-align:justify\">Chỉ cần c&oacute; một v&agrave;i kiểu ảnh chất lượng v&agrave; m&ocirc; tả c&aacute;c th&ocirc;ng tin chi tiết về sản phẩm để quảng b&aacute;, chắc hẳn đ&acirc;y sẽ l&agrave; thị trường rất tiềm năng.</p>\r\n\r\n<p style=\"text-align:justify\">&gt;&gt;&gt;&nbsp;<a href=\"https://www.uplevo.com/blog/kinh-doanh/ban-hang-online-hieu-qua/\" rel=\"noopener noreferrer\" target=\"_blank\">B&iacute; k&iacute;p b&aacute;n h&agrave;ng online hiệu quả từ A-Z</a></p>\r\n\r\n<h3 style=\"text-align:justify\">3. Li&ecirc;n kết với c&aacute;c cửa h&agrave;ng kh&aacute;c</h3>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i ra c&ograve;n c&oacute; một kinh nghiệm kh&aacute; hay l&agrave; li&ecirc;n kết với c&aacute;c cửa h&agrave;ng b&aacute;n điện thoại, m&aacute;y t&iacute;nh bảng,&hellip; để tăng doanh thu b&aacute;n h&agrave;ng. Khi sắm cho m&igrave;nh một chiếc điện thoại di động mới, kh&aacute;ch h&agrave;ng thường rất quan t&acirc;m đến c&aacute;c phụ ki&ecirc;n đi k&egrave;m để vừa bảo vệ lại vừa l&agrave;m đẹp th&ecirc;m cho ch&uacute; dế của m&igrave;nh.</p>\r\n\r\n<p style=\"text-align:justify\">Do đ&oacute;, nếu bạn c&oacute; thể kết hợp với cơ sở đấy để trưng b&aacute;n sản phẩm của m&igrave;nh th&igrave; đ&oacute; kh&ocirc;ng phải l&agrave; một &yacute; tưởng tồi. Việc kh&oacute; khăn chỉ l&agrave; hai b&ecirc;n phải thoả thuận với nhau cho c&ugrave;ng c&oacute; lợi v&agrave; hợp l&yacute; hợp t&igrave;nh th&ocirc;i.</p>\r\n\r\n<h3 style=\"text-align:justify\">4. Quản l&yacute; sản phẩm hiệu quả</h3>\r\n\r\n<p style=\"text-align:justify\">Một vấn đề nữa m&agrave; c&aacute;c chủ cửa h&agrave;ng cũng rất đ&aacute;ng lưu t&acirc;m l&agrave; l&agrave;m thế n&agrave;o để quản l&yacute; được to&agrave;n bộ sản phẩm trong cửa h&agrave;ng. Quản l&yacute; sao cho cẩn thận v&agrave; ch&iacute;nh x&aacute;c nhất bởi số lượng h&agrave;ng ho&aacute; trong c&aacute;c cửa h&agrave;ng kinh doanh phụ kiện điện thoại kh&ocirc;ng phải l&agrave; một con số nhỏ.</p>\r\n\r\n<p style=\"text-align:justify\">Từ một d&ograve;ng điện thoại c&oacute; rất nhiều phụ kiện đi k&egrave;m. Với v&ocirc; số kiểu mẫu m&atilde; rồi kiểu c&aacute;ch kh&aacute;c nhau. Chưa kể đến c&oacute; rất nhiều d&ograve;ng điện thoại kh&aacute;c nhau v&agrave; đ&ocirc;i khi nh&igrave;n bằng mắt thường dễ bị nhầm lần k&iacute;ch thước giữa c&aacute;c loại. Mỗi l&uacute;c cần t&igrave;m kiếm hoặc kiểm tra h&agrave;ng ho&aacute; sẽ mất rất nhiều thời gian v&agrave; kh&oacute; ch&iacute;nh x&aacute;c.</p>\r\n\r\n<p style=\"text-align:justify\"><img alt=\"quản lý sản phẩm một cách hiệu quả\" src=\"https://www.uplevo.com/blog/wp-content/uploads/2019/07/quan-ly-san-pham-mot-cach-hieu-qua.jpg\" style=\"height:471px; width:876px\" /></p>\r\n\r\n<p style=\"text-align:justify\">V&igrave; thế c&oacute; một lời khuy&ecirc;n ch&acirc;n th&agrave;nh l&agrave; c&aacute;c chủ cửa h&agrave;ng n&ecirc;n sử dụng phần mềm quản l&yacute; ưu việt. Sử dụng đơn giản, dễ d&agrave;ng tra cứu, nhập số liệu, kiểm tra đơn h&agrave;ng,&hellip;v&ocirc; c&ugrave;ng thuận tiện. Từ đ&oacute; bạn c&oacute; c&aacute;i nh&igrave;n tổng quan hơn về t&igrave;nh h&igrave;nh kinh doanh của m&igrave;nh, h&agrave;ng ng&agrave;y cập nh&acirc;t được số lượng b&aacute;n ra v&agrave; hiện c&oacute;. Tr&aacute;nh thất tho&aacute;t h&agrave;ng ho&aacute; đ&aacute;ng kể trong mỗi th&aacute;ng.</p>\r\n\r\n<p style=\"text-align:justify\">&gt;&gt;&gt;&nbsp;<a href=\"http://www.uplevo.com/designbox/phan-mem-quan-ly-ban-hang\">Top 15 Phần mềm quản l&yacute; b&aacute;n h&agrave;ng tố t nhất</a></p>\r\n\r\n<p style=\"text-align:justify\">D&ugrave; c&oacute; kh&ocirc;ng &iacute;t kh&oacute; khăn v&agrave; sự cạnh tranh lớn tuy nhi&ecirc;n kinh doanh phụ kiện điện thoại lu&ocirc;n l&agrave; một &yacute; tưởng kinh doanh đầy sức h&uacute;t. Với &yacute; tưởng kinh doanh v&ocirc; c&ugrave;ng tiềm năng n&agrave;y, ch&uacute;ng t&ocirc;i đ&atilde; tổng hợp v&agrave; ph&acirc;n t&iacute;ch những yếu tố để bạn đọc c&oacute; th&ecirc;m kinh nghiệm khi bắt đầu kinh doanh.</p>\r\n\r\n<p style=\"text-align:justify\">Mong rằng c&aacute;c bạn sẽ thấy những th&ocirc;ng tin tr&ecirc;n l&agrave; hữu &iacute;ch v&agrave; hiệu quả. Ch&uacute;c c&aacute;c bạn th&agrave;nh c&ocirc;ng.</p>\r\n\r\n<p>&nbsp;</p>', '2020-08-17 10:27:10', 1, 1, '2020-08-17 10:37:01');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `atb_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `atb_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `atb_type` tinyint(4) NOT NULL DEFAULT '0',
  `atb_category_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `atb_name`, `atb_slug`, `atb_type`, `atb_category_id`, `created_at`, `updated_at`) VALUES
(1, 'hkh', 'hkh', 2, 1, '2020-10-10 09:34:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_parent_id` int(11) NOT NULL DEFAULT '0',
  `c_hot` tinyint(4) NOT NULL DEFAULT '0',
  `c_status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `c_name`, `c_slug`, `c_avatar`, `c_banner`, `c_description`, `c_parent_id`, `c_hot`, `c_status`, `created_at`, `updated_at`) VALUES
(15, 'Bao da, ốp lưng', 'bao-da-op-lung', NULL, NULL, NULL, 0, 1, 1, '2020-08-17 02:12:43', '2020-08-17 10:03:41'),
(16, 'Cáp sạc', 'cap-sac', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 02:12:54', '2020-08-17 10:03:45'),
(17, 'Sạc Pin dự phòng', 'sac-pin-du-phong', NULL, NULL, NULL, 0, 1, 1, '2020-08-17 02:13:11', '2020-08-17 10:03:44'),
(18, 'Thiết bị mạng', 'thiet-bi-mang', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 02:13:36', NULL),
(19, 'Camera', 'camera', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 02:13:44', NULL),
(20, 'Chuột, bàn phím', 'chuot-ban-phim', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 02:13:53', NULL),
(28, 'Thiết bị Iphone', 'thiet-bi-iphone', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 04:14:41', NULL),
(29, 'Giá đỡ điện thoại, laptop', 'gia-do-dien-thoai-laptop', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 07:35:18', NULL),
(30, 'Quạt mini', 'quat-mini', NULL, NULL, NULL, 0, 0, 1, '2020-08-17 07:35:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cmt_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cmt_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cmt_content` text COLLATE utf8mb4_unicode_ci,
  `cmt_parent_id` int(11) NOT NULL DEFAULT '0',
  `cmt_product_id` int(11) NOT NULL DEFAULT '0',
  `cmt_admin_id` int(11) NOT NULL DEFAULT '0',
  `cmt_user_id` int(11) NOT NULL DEFAULT '0',
  `cmt_like` int(11) NOT NULL DEFAULT '0',
  `cmt_disk_like` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_phone` char(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `e_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `e_position_1` tinyint(4) NOT NULL DEFAULT '0',
  `e_position_2` tinyint(4) NOT NULL DEFAULT '0',
  `e_position_3` tinyint(4) NOT NULL DEFAULT '0',
  `e_position_4` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `e_name`, `e_banner`, `e_link`, `e_position_1`, `e_position_2`, `e_position_3`, `e_position_4`, `created_at`, `updated_at`) VALUES
(1, 'Robot hút bụi Xiaomi Vacuum Mop Pro', '2020-08-17__xiaomi-mi-robot-vacuum-mop-pro-595x100-cat-13.webp', 'https://phukien.phupt.net/dan-muc/bao-da-op-lung-15', 1, 0, 0, 0, '2020-08-17 10:36:05', '2020-08-17 10:36:05'),
(2, 'Ốp lưng Iphone', '2020-08-17__1190x200-6.webp', 'https://phukien.phupt.net/dan-muc/bao-da-op-lung-15', 0, 1, 0, 0, '2020-08-17 10:36:14', '2020-08-17 10:36:14'),
(3, 'Miếng dán cường lực cho iPhone XS Max', '2020-08-17__cuong-luc-xs-max-595x100.webp', 'https://phukien.phupt.net/san-pham/mieng-dan-ppf-full-man-hinh-cao-cap-cho-samsung-galaxy-note-10-plus-9', 0, 0, 1, 0, '2020-08-17 10:36:21', '2020-08-17 10:36:21'),
(4, 'Pin sạc dự phòng anker', '2020-08-17__anker-16.webp', 'https://phukien.phupt.net/dan-muc/sac-pin-du-phong-17', 0, 0, 0, 1, '2020-08-17 10:36:28', '2020-08-17 10:36:28');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_entered`
--

CREATE TABLE `invoice_entered` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ie_suppliere` int(11) NOT NULL DEFAULT '0',
  `ie_name_product` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ie_total_money` int(11) NOT NULL DEFAULT '0',
  `ie_number` int(11) NOT NULL DEFAULT '0',
  `ie_number_sold` int(11) NOT NULL DEFAULT '0',
  `ie_product_id` int(11) NOT NULL DEFAULT '0',
  `ie_money` int(11) NOT NULL DEFAULT '0',
  `ie_status` tinyint(4) NOT NULL DEFAULT '0',
  `ie_meta` text COLLATE utf8mb4_unicode_ci,
  `ie_the_advance` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `k_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `k_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `k_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `k_hot` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mn_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mn_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mn_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mn_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mn_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mn_hot` tinyint(4) NOT NULL DEFAULT '0',
  `mn_status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `mn_name`, `mn_slug`, `mn_avatar`, `mn_banner`, `mn_description`, `mn_hot`, `mn_status`, `created_at`, `updated_at`) VALUES
(1, 'Tuyển dụng', 'tuyen-dung', NULL, NULL, NULL, 0, 1, '2020-08-16 20:12:09', NULL),
(2, 'Kinh nghiệm kinh doanh', 'kinh-nghiem-kinh-doanh', NULL, NULL, NULL, 0, 1, '2020-08-17 10:14:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_02_02_041429_create_categories_table', 1),
(4, '2020_02_02_155318_create_keywords_table', 1),
(5, '2020_02_03_145303_create_products_table', 1),
(6, '2020_02_06_165057_create_attributes_table', 1),
(7, '2020_02_06_184708_create_products_attributes_table', 1),
(8, '2020_02_08_005029_add_multiple_column_attribute_in_table_products', 1),
(9, '2020_02_09_073958_create_transactions_table', 1),
(10, '2020_02_09_074025_create_orders_table', 1),
(11, '2020_02_09_133309_create_products_keywords_table', 1),
(12, '2020_02_09_155308_create_admins_table', 1),
(13, '2020_02_09_180519_create_menus_table', 1),
(14, '2020_02_09_180620_create_articles_table', 1),
(15, '2020_02_12_100000_create_password_resets_table', 1),
(16, '2020_02_13_154148_alter_column_pro_number_in_table_product', 1),
(17, '2020_02_13_171036_create_slides_table', 1),
(18, '2020_02_14_121248_alter_column_a_position_in_table_articles', 1),
(19, '2020_02_15_053225_create_user_favourite_table', 1),
(20, '2020_02_15_191555_create_ratings_table', 1),
(21, '2020_02_17_162605_create_events_table', 1),
(22, '2020_02_18_152103_create_product_images_table', 1),
(23, '2020_02_24_222836_create_social_accounts_table', 1),
(24, '2020_03_08_104810_create_statics_table', 1),
(25, '2020_03_08_213403_alter_column_pro_age_review_in_table_product', 1),
(26, '2020_03_12_205704_create_contacts_table', 1),
(27, '2020_03_17_183239_create_comments_table', 1),
(28, '2020_03_22_003200_alter_column_spam_comment_ratings_in_table_users', 1),
(29, '2020_03_23_223714_alter_column_admin_in_table_admin', 1),
(30, '2020_03_24_201555_alter_column_c_parent_id_in_table_categories', 1),
(31, '2020_03_25_214331_create_list_table_system_pay_table', 1),
(32, '2020_03_27_181534_alter_column_type_pay_in_table_transaction', 1),
(33, '2020_04_14_164245_create_supplieres_table', 1),
(34, '2020_04_15_003305_alter_column_pro_supplier_id_in_table_products', 1),
(35, '2020_04_16_234410_after_column_tst_admin_id_in_table_transaction', 1),
(36, '2020_04_29_104806_alter_column_pro_expiration_date_in_table_products', 1),
(37, '2020_04_29_112931_create_invoice_entered_in_tables', 1),
(38, '2020_06_17_192357_create_product_invoice_entered_table', 1),
(39, '2020_06_21_112319_create_permission_tables', 1),
(40, '2020_06_21_205241_create_activity_log_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `od_transaction_id` int(11) NOT NULL DEFAULT '0',
  `od_product_id` int(11) NOT NULL DEFAULT '0',
  `od_sale` int(11) NOT NULL DEFAULT '0',
  `od_qty` tinyint(4) NOT NULL DEFAULT '0',
  `od_price` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `od_transaction_id`, `od_product_id`, `od_sale`, `od_qty`, `od_price`, `created_at`, `updated_at`) VALUES
(1, 1, 77, 0, 2, 250000, '2020-08-17 10:06:59', NULL),
(2, 2, 83, 5, 1, 807500, '2020-10-10 08:27:43', NULL),
(3, 3, 77, 0, 1, 250000, '2020-10-28 09:22:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_histories`
--

CREATE TABLE `pay_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `ph_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ph_user_id` int(10) UNSIGNED NOT NULL,
  `ph_credit` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ph_debit` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ph_balance` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ph_meta_detail` text COLLATE utf8mb4_unicode_ci,
  `ph_status` tinyint(4) NOT NULL DEFAULT '0',
  `ph_month` tinyint(3) UNSIGNED DEFAULT NULL,
  `ph_year` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_ins`
--

CREATE TABLE `pay_ins` (
  `id` int(10) UNSIGNED NOT NULL,
  `pi_user_id` int(10) UNSIGNED NOT NULL,
  `pi_admin_id` int(10) UNSIGNED NOT NULL,
  `pi_provider` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pi_money` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pi_fee` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pi_amount` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pi_meta_detail` text COLLATE utf8mb4_unicode_ci,
  `pi_status` tinyint(4) NOT NULL DEFAULT '0',
  `pi_month` tinyint(3) UNSIGNED DEFAULT NULL,
  `pi_year` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_outs`
--

CREATE TABLE `pay_outs` (
  `id` int(10) UNSIGNED NOT NULL,
  `po_user_id` int(10) UNSIGNED NOT NULL,
  `po_transaction_id` int(10) UNSIGNED NOT NULL,
  `po_money` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `po_meta_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `po_status` tinyint(4) NOT NULL DEFAULT '0',
  `po_month` tinyint(3) UNSIGNED DEFAULT NULL,
  `po_year` smallint(5) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_permission` tinyint(4) NOT NULL DEFAULT '0',
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `group_permission`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'nguyen-van-a', NULL, 0, 'admins', '2020-10-10 09:28:50', '2020-10-10 09:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pro_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_price` int(11) NOT NULL DEFAULT '0',
  `pro_price_entry` int(11) NOT NULL DEFAULT '0' COMMENT 'giá nhập',
  `pro_category_id` int(11) NOT NULL DEFAULT '0',
  `pro_supplier_id` int(11) NOT NULL DEFAULT '0',
  `pro_admin_id` int(11) NOT NULL DEFAULT '0',
  `pro_sale` tinyint(4) NOT NULL DEFAULT '0',
  `pro_expiration_date` tinyint(4) NOT NULL DEFAULT '10',
  `pro_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_view` int(11) NOT NULL DEFAULT '0',
  `pro_hot` tinyint(4) NOT NULL DEFAULT '0',
  `pro_active` tinyint(4) NOT NULL DEFAULT '1',
  `pro_pay` int(11) NOT NULL DEFAULT '0',
  `pro_number_import` int(11) NOT NULL DEFAULT '0',
  `pro_import_goods` int(11) NOT NULL DEFAULT '0',
  `pro_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `pro_content` text COLLATE utf8mb4_unicode_ci,
  `pro_review_total` int(11) NOT NULL DEFAULT '0',
  `pro_review_star` int(11) NOT NULL DEFAULT '0',
  `pro_age_review` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `pro_expiration` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_number` int(11) NOT NULL DEFAULT '0',
  `pro_resistant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_energy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_country` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pro_name`, `pro_slug`, `pro_price`, `pro_price_entry`, `pro_category_id`, `pro_supplier_id`, `pro_admin_id`, `pro_sale`, `pro_expiration_date`, `pro_avatar`, `pro_view`, `pro_hot`, `pro_active`, `pro_pay`, `pro_number_import`, `pro_import_goods`, `pro_description`, `pro_content`, `pro_review_total`, `pro_review_star`, `pro_age_review`, `created_at`, `pro_expiration`, `pro_number`, `pro_resistant`, `pro_energy`, `pro_country`, `updated_at`) VALUES
(1, 'Tai nghe Bluetooth Apple AirPods 2 VN/A', 'tai-nghe-bluetooth-apple-airpods-2-vna', 3650000, 0, 28, 0, 0, 2, 10, '2020-08-17__12.webp', 0, 0, 1, 0, 3, 0, 'Tai nghe Bluetooth Apple AirPods 2 VN/A', 'Tai nghe Bluetooth Apple AirPods 2 VN/A', 0, 0, 0, '2020-08-17 02:34:19', '2020-08-17', 3, NULL, NULL, 0, '2020-08-17 08:27:49'),
(2, 'Bút cảm ứng Apple Pencil 2 MU8F2', 'but-cam-ung-apple-pencil-2-mu8f2', 3900000, 0, 28, 0, 0, 2, 10, '2020-08-17__2.webp', 0, 0, 1, 0, 4, 0, 'Bút cảm ứng Apple Pencil 2 MU8F2', 'Bút cảm ứng Apple Pencil 2 MU8F2', 0, 0, 0, '2020-08-17 02:35:16', '2020-08-17', 4, NULL, NULL, 0, '2020-08-17 04:14:54'),
(3, 'Bút Apple Pencil 1', 'but-apple-pencil-1', 3200000, 0, 28, 0, 0, 2, 10, '2020-08-17__3.webp', 0, 0, 1, 0, 2, 0, 'Bút Apple Pencil 1', 'Bút Apple Pencil 1', 0, 0, 0, '2020-08-17 02:36:27', '2020-08-17', 2, NULL, NULL, 0, '2020-08-17 04:15:19'),
(4, 'Bàn phím Smart Keyboard cho iPad Pro 10.5/Air 10.5/10.2', 'ban-phim-smart-keyboard-cho-ipad-pro-105air-105102', 3800000, 0, 28, 0, 0, 1, 10, '2020-08-17__4.jpg', 0, 0, 1, 0, 3, 0, 'Bàn phím Smart Keyboard cho iPad Pro 10.5/Air 10.5/10.2', 'Bàn phím Smart Keyboard cho iPad Pro 10.5/Air 10.5/10.2', 0, 0, 0, '2020-08-17 02:37:39', '2020-08-17', 3, NULL, NULL, 0, '2020-08-17 04:15:06'),
(5, 'Chuột Apple Magic Mouse 2', 'chuot-apple-magic-mouse-2', 2000000, 0, 20, 0, 0, 1, 10, '2020-08-17__5.jpg', 1, 0, 1, 0, 5, 0, 'Chuột Apple Magic Mouse 2', 'Chuột Apple Magic Mouse 2', 0, 0, 0, '2020-08-17 02:38:29', '2020-08-17', 5, NULL, NULL, 0, '2020-08-17 04:13:13'),
(6, 'Cáp Lightning Apple 1m MQUE2 Chính hãng', 'cap-lightning-apple-1m-mque2-chinh-hang', 490000, 0, 28, 0, 0, 2, 10, '2020-08-17__6.jpg', 0, 0, 1, 0, 4, 0, 'Cáp Lightning Apple 1m MQUE2 Chính hãng', 'Cáp Lightning Apple 1m MQUE2 Chính hãng', 0, 0, 0, '2020-08-17 02:39:05', '2020-08-17', 4, NULL, NULL, 0, '2020-08-17 04:15:31'),
(7, 'Sạc Macbook Apple 61W USB-C Power Adapter MNF72 Chính hãng', 'sac-macbook-apple-61w-usb-c-power-adapter-mnf72-chinh-hang', 2220000, 0, 28, 0, 0, 4, 10, '2020-08-17__7.webp', 0, 0, 1, 0, 4, 0, 'Sạc Macbook Apple 61W USB-C Power Adapter MNF72 Chính hãng', 'Sạc Macbook Apple 61W USB-C Power Adapter MNF72 Chính hãng', 0, 0, 0, '2020-08-17 02:39:41', '2020-08-17', 4, NULL, NULL, 0, '2020-08-17 04:15:41'),
(8, 'Miếng dán Jcpal bảo vệ Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max', 'mieng-dan-jcpal-bao-ve-camera-sau-cho-iphone-11-11-pro-11-pro-max', 450000, 0, 28, 0, 0, 0, 10, '2020-08-17__8.webp', 0, 0, 1, 0, 3, 0, 'Miếng dán Jcpal bảo vệ Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max', 'Miếng dán Jcpal bảo vệ Camera sau cho iPhone 11 - 11 Pro - 11 Pro Max', 0, 0, 0, '2020-08-17 02:47:43', '2020-08-17', 3, NULL, NULL, 0, '2020-08-17 04:16:08'),
(9, 'Miếng Dán PPF Full Màn hình Cao cấp cho Samsung Galaxy Note 10 Plus', 'mieng-dan-ppf-full-man-hinh-cao-cap-cho-samsung-galaxy-note-10-plus', 180000, 0, 28, 0, 0, 0, 10, '2020-08-17__9.jpg', 4, 0, 1, 0, 2, 0, 'Miếng Dán PPF Full Màn hình Cao cấp cho Samsung Galaxy Note 10 Plus', 'Miếng Dán PPF Full Màn hình Cao cấp cho Samsung Galaxy Note 10 Plus', 0, 0, 0, '2020-08-17 03:49:20', '2020-08-17', 2, NULL, NULL, 0, '2020-08-17 04:16:55'),
(10, 'Miếng dán cường lực chống nhìn trộm Mocoll Full 3D cho iPhone X / Xs', 'mieng-dan-cuong-luc-chong-nhin-trom-mocoll-full-3d-cho-iphone-x-xs', 250000, 0, 28, 0, 0, 1, 10, '2020-08-17__10.jpg', 1, 0, 1, 0, 1, 0, 'Miếng dán cường lực chống nhìn trộm Mocoll Full 3D cho iPhone X / Xs', 'Miếng dán cường lực chống nhìn trộm Mocoll Full 3D cho iPhone X / Xs', 0, 0, 0, '2020-08-17 03:50:08', '2020-08-17', 1, NULL, NULL, 0, '2020-08-17 04:16:44'),
(11, 'Miếng dán PPF Full viền mặt sau cho iPhone X/XS', 'mieng-dan-ppf-full-vien-mat-sau-cho-iphone-xxs', 350000, 0, 28, 0, 0, 1, 10, '2020-08-17__11.webp', 0, 0, 1, 0, 2, 0, 'Miếng dán PPF Full viền mặt sau cho iPhone X/XS', 'Miếng dán PPF Full viền mặt sau cho iPhone X/XS', 0, 0, 0, '2020-08-17 03:50:54', '2020-08-17', 2, NULL, NULL, 0, '2020-08-17 04:16:22'),
(12, 'Dán cường lực cho iPhone XS Max - JCPal Full Đen Cao Cấp', 'dan-cuong-luc-cho-iphone-xs-max-jcpal-full-den-cao-cap', 150000, 0, 28, 0, 0, 1, 10, '2020-08-17__12.jpg', 0, 0, 1, 0, 3, 0, 'Dán cường lực cho iPhone XS Max - JCPal Full Đen Cao Cấp', 'Dán cường lực cho iPhone XS Max - JCPal Full Đen Cao Cấp', 0, 0, 0, '2020-08-17 03:51:31', '2020-08-17', 3, NULL, NULL, 0, '2020-08-17 04:16:34'),
(13, 'Dán cường lực cho iPhone 11 Pro - Mocoll Full Đen Cao Cấp', 'dan-cuong-luc-cho-iphone-11-pro-mocoll-full-den-cao-cap', 210000, 0, 28, 0, 0, 3, 10, '2020-08-17__13.jpg', 0, 0, 1, 0, 2, 0, 'Dán cường lực cho iPhone 11 Pro - Mocoll Full Đen Cao Cấp', 'Dán cường lực cho iPhone 11 Pro - Mocoll Full Đen Cao Cấp', 0, 0, 0, '2020-08-17 03:52:08', '2020-08-17', 2, NULL, NULL, 0, '2020-08-17 04:15:56'),
(14, 'Bao da kiêm Bàn phím cho Samsung Galaxy Tab S6', 'bao-da-kiem-ban-phim-cho-samsung-galaxy-tab-s6', 230000, 0, 15, 0, 0, 1, 10, '2020-08-17__14.jpg', 0, 0, 1, 0, 4, 0, 'Bao da kiêm Bàn phím cho Samsung Galaxy Tab S6', 'Bao da kiêm Bàn phím cho Samsung Galaxy Tab S6', 0, 0, 0, '2020-08-17 03:52:49', '2020-08-17', 4, NULL, NULL, 0, '2020-08-17 03:53:11'),
(15, 'Bao Da Clear View cho Samsung Galaxy S20 Ultra', 'bao-da-clear-view-cho-samsung-galaxy-s20-ultra', 792000, 0, 15, 0, 0, 0, 10, '2020-08-17__15.webp', 1, 0, 1, 0, 3, 0, 'Bao Da Clear View cho Samsung Galaxy S20 Ultra', 'Bao Da Clear View cho Samsung Galaxy S20 Ultra', 0, 0, 0, '2020-08-17 03:56:15', '2020-08-17', 3, NULL, NULL, 0, NULL),
(16, 'Ốp lưng ESR Yippee Color Cho iPhone 11 Pro Max', 'op-lung-esr-yippee-color-cho-iphone-11-pro-max', 250000, 0, 15, 0, 0, 0, 10, '2020-08-17__16.jpg', 0, 0, 1, 0, 2, 0, 'Ốp lưng ESR Yippee Color Cho iPhone 11 Pro Max', 'Ốp lưng ESR Yippee Color Cho iPhone 11 Pro Max', 0, 0, 0, '2020-08-17 03:56:50', '2020-08-17', 2, NULL, NULL, 0, NULL),
(17, 'Ốp lưng S-Case trong nhám cho iPhone 11', 'op-lung-s-case-trong-nham-cho-iphone-11', 250000, 0, 15, 0, 0, 3, 10, '2020-08-17__17.webp', 0, 0, 1, 0, 4, 0, 'Ốp lưng S-Case trong nhám cho iPhone 11', 'Ốp lưng S-Case trong nhám cho iPhone 11', 0, 0, 0, '2020-08-17 03:57:19', '2020-08-17', 4, NULL, NULL, 0, NULL),
(18, 'Ốp lưng Spigen Ultra Hybrid S cho iPhone 11 Pro', 'op-lung-spigen-ultra-hybrid-s-cho-iphone-11-pro', 650000, 0, 15, 0, 0, 0, 10, '2020-08-17__18.jpg', 0, 0, 1, 0, 4, 0, 'Ốp lưng Spigen Ultra Hybrid S cho iPhone 11 Pro', 'Ốp lưng Spigen Ultra Hybrid S cho iPhone 11 Pro', 0, 0, 0, '2020-08-17 03:57:53', '2020-08-17', 4, NULL, NULL, 0, NULL),
(19, 'Sạc Aukey 24W 2 cổng USB AIpower PA-U42', 'sac-aukey-24w-2-cong-usb-aipower-pa-u42', 350000, 0, 16, 0, 0, 0, 10, '2020-08-17__19.jpg', 0, 0, 1, 0, 3, 0, 'Sạc Aukey 24W 2 cổng USB AIpower PA-U42', 'Sạc Aukey 24W 2 cổng USB AIpower PA-U42', 0, 0, 0, '2020-08-17 04:00:31', '2020-08-17', 3, NULL, NULL, 0, NULL),
(20, 'Sạc Anker 1 Cổng 18W PD A2019', 'sac-anker-1-cong-18w-pd-a2019', 450000, 0, 16, 0, 0, 0, 10, '2020-08-17__1.webp', 0, 0, 1, 0, 2, 0, 'Sạc Anker 1 Cổng 18W PD A2019', 'Sạc Anker 1 Cổng 18W PD A2019', 0, 0, 0, '2020-08-17 04:01:16', '2020-08-17', 2, NULL, NULL, 0, NULL),
(21, 'Sạc Anker Powerport II 2 Cổng 24W A2027', 'sac-anker-powerport-ii-2-cong-24w-a2027', 250000, 0, 16, 0, 0, 0, 10, '2020-08-17__2.webp', 0, 0, 1, 0, 3, 0, 'Sạc Anker Powerport II 2 Cổng 24W A2027', 'Sạc Anker Powerport II 2 Cổng 24W A2027', 0, 0, 0, '2020-08-17 04:01:39', '2020-08-17', 3, NULL, NULL, 0, NULL),
(22, 'Sạc Titan 1A CB02 dành cho iPhone kèm cáp Lightning Trắng', 'sac-titan-1a-cb02-danh-cho-iphone-kem-cap-lightning-trang', 360000, 0, 16, 0, 0, 2, 10, '2020-08-17__3.jpg', 0, 0, 1, 0, 2, 0, 'Sạc Titan 1A CB02 dành cho iPhone kèm cáp Lightning Trắng', 'Sạc Titan 1A CB02 dành cho iPhone kèm cáp Lightning Trắng', 0, 0, 0, '2020-08-17 04:02:10', '2020-08-17', 2, NULL, NULL, 0, NULL),
(23, 'Cáp Anker PowerLine II Lightning (3FT/0.9M) A8432', 'cap-anker-powerline-ii-lightning-3ft09m-a8432', 250000, 0, 16, 0, 0, 0, 10, '2020-08-17__4.webp', 0, 0, 1, 0, 3, 0, 'Cáp Anker PowerLine II Lightning (3FT/0.9M) A8432', 'Cáp Anker PowerLine II Lightning (3FT/0.9M) A8432', 0, 0, 0, '2020-08-17 04:03:01', '2020-08-17', 3, NULL, NULL, 0, NULL),
(24, 'Pin sạc dự phòng ENERGIZER 10,000MAH - UE10045', 'pin-sac-du-phong-energizer-10000mah-ue10045', 250000, 0, 17, 0, 0, 0, 10, '2020-08-17__5.jpg', 1, 0, 1, 0, 3, 0, 'Pin sạc dự phòng ENERGIZER 10,000MAH - UE10045', 'Pin sạc dự phòng ENERGIZER 10,000MAH - UE10045', 0, 0, 0, '2020-08-17 04:03:35', '2020-08-17', 3, NULL, NULL, 0, NULL),
(25, 'Pin dự phòng Samsung EB-P1100 10.000 Mah cổng USB-C', 'pin-du-phong-samsung-eb-p1100-10000-mah-cong-usb-c', 360000, 0, 17, 0, 0, 0, 10, '2020-08-17__6.webp', 0, 0, 1, 0, 3, 0, 'Pin dự phòng Samsung EB-P1100 10.000 Mah cổng USB-C', 'Pin dự phòng Samsung EB-P1100 10.000 Mah cổng USB-C', 0, 0, 0, '2020-08-17 04:04:00', '2020-08-17', 3, NULL, NULL, 0, NULL),
(26, 'Pin sạc dự phòng Xiaomi Redmi 20000mah sạc nhanh 18W', 'pin-sac-du-phong-xiaomi-redmi-20000mah-sac-nhanh-18w', 350000, 0, 17, 0, 0, 0, 10, '2020-08-17__7.jpg', 0, 0, 1, 0, 3, 0, 'Pin sạc dự phòng Xiaomi Redmi 20000mah sạc nhanh 18W', 'Pin sạc dự phòng Xiaomi Redmi 20000mah sạc nhanh 18W', 0, 0, 0, '2020-08-17 04:05:40', '2020-08-17', 3, NULL, NULL, 0, NULL),
(27, 'Pin sạc dự phòng Polymer 10.000mAh Xiaomi Mi 18W Fast Charge Power Bank 3', 'pin-sac-du-phong-polymer-10000mah-xiaomi-mi-18w-fast-charge-power-bank-3', 490000, 0, 17, 0, 0, 10, 10, '2020-08-17__8.jpg', 0, 0, 1, 0, 8, 0, 'Pin sạc dự phòng Polymer 10.000mAh Xiaomi Mi 18W Fast Charge Power Bank 3', 'Pin sạc dự phòng Polymer 10.000mAh Xiaomi Mi 18W Fast Charge Power Bank 3', 0, 0, 0, '2020-08-17 04:06:09', '2020-08-17', 8, NULL, NULL, 0, NULL),
(28, 'Pin dự phòng AUKEY PB-N42 Pocket 10000 mAh', 'pin-du-phong-aukey-pb-n42-pocket-10000-mah', 330000, 0, 17, 0, 0, 10, 10, '2020-08-17__9.jpg', 2, 0, 1, 0, 13, 0, 'Pin dự phòng AUKEY PB-N42 Pocket 10000 mAh', 'Pin dự phòng AUKEY PB-N42 Pocket 10000 mAh', 0, 0, 0, '2020-08-17 04:06:36', '2020-08-17', 13, NULL, NULL, 0, NULL),
(29, 'Sạc dự phòng Energizer UE15032PQ 15000mah', 'sac-du-phong-energizer-ue15032pq-15000mah', 490000, 0, 17, 0, 0, 20, 10, '2020-08-17__10.webp', 2, 0, 1, 0, 16, 0, 'Sạc dự phòng Energizer UE15032PQ 15000mah', 'Sạc dự phòng Energizer UE15032PQ 15000mah', 0, 0, 0, '2020-08-17 04:07:10', '2020-08-17', 16, NULL, NULL, 0, NULL),
(30, 'Bộ kích sóng Wifi Xiaomi Pro', 'bo-kich-song-wifi-xiaomi-pro', 360000, 0, 18, 0, 0, 25, 10, '2020-08-17__1.webp', 0, 0, 1, 0, 17, 0, 'Bộ kích sóng Wifi Xiaomi Pro', 'Bộ kích sóng Wifi Xiaomi Pro', 0, 0, 0, '2020-08-17 04:08:11', '2020-08-17', 17, NULL, NULL, 0, NULL),
(31, 'Thiết bị mở rộng sóng Wifi TOTOLINK EX201', 'thiet-bi-mo-rong-song-wifi-totolink-ex201', 250000, 0, 18, 0, 0, 25, 10, '2020-08-17__2.jpg', 0, 0, 1, 0, 20, 0, 'Thiết bị mở rộng sóng Wifi TOTOLINK EX201', 'Thiết bị mở rộng sóng Wifi TOTOLINK EX201', 0, 0, 0, '2020-08-17 04:08:41', '2020-08-17', 20, NULL, NULL, 0, NULL),
(32, 'Router Wi-Fi TP-LINK Băng tần kép, Tốc độ cao TP-LINK Archer C50', 'router-wi-fi-tp-link-bang-tan-kep-toc-do-cao-tp-link-archer-c50', 350000, 0, 18, 0, 0, 11, 10, '2020-08-17__3.jpg', 0, 0, 1, 0, 11, 0, 'Router Wi-Fi TP-LINK Băng tần kép, Tốc độ cao TP-LINK Archer C50', 'Router Wi-Fi TP-LINK Băng tần kép, Tốc độ cao TP-LINK Archer C50', 0, 0, 0, '2020-08-17 04:09:13', '2020-08-17', 11, NULL, NULL, 0, NULL),
(33, 'Hệ thống Wi-Fi Mesh cho toàn ngôi nhà AC1300 Tp-Link Deco M5', 'he-thong-wi-fi-mesh-cho-toan-ngoi-nha-ac1300-tp-link-deco-m5', 650000, 0, 18, 0, 0, 10, 10, '2020-08-17__4.jpg', 0, 0, 1, 0, 13, 0, 'Hệ thống Wi-Fi Mesh cho toàn ngôi nhà AC1300 Tp-Link Deco M5', 'Hệ thống Wi-Fi Mesh cho toàn ngôi nhà AC1300 Tp-Link Deco M5', 0, 0, 0, '2020-08-17 04:09:40', '2020-08-17', 13, NULL, NULL, 0, NULL),
(34, 'Hệ thống Wi-Fi Mesh cho toàn ngôi nhà AC1200 Tp-Link Deco M4', 'he-thong-wi-fi-mesh-cho-toan-ngoi-nha-ac1200-tp-link-deco-m4', 650000, 0, 18, 0, 0, 4, 10, '2020-08-17__5.jpg', 0, 0, 1, 0, 8, 0, 'Hệ thống Wi-Fi Mesh cho toàn ngôi nhà AC1200 Tp-Link Deco M4', 'Hệ thống Wi-Fi Mesh cho toàn ngôi nhà AC1200 Tp-Link Deco M4', 0, 0, 0, '2020-08-17 04:10:02', '2020-08-17', 8, NULL, NULL, 0, NULL),
(35, 'Thiết bị mở rộng sóng Wifi chuẩn N tốc độ 300MBPS Mercusys  MW300RE', 'thiet-bi-mo-rong-song-wifi-chuan-n-toc-do-300mbps-mercusys-mw300re', 550000, 0, 18, 0, 0, 6, 10, '2020-08-17__6.jpg', 3, 0, 1, 0, 7, 0, 'Thiết bị mở rộng sóng Wifi chuẩn N tốc độ 300MBPS Mercusys  MW300RE', 'Thiết bị mở rộng sóng Wifi chuẩn N tốc độ 300MBPS Mercusys  MW300RE', 0, 0, 0, '2020-08-17 04:10:27', '2020-08-17', 7, NULL, NULL, 0, NULL),
(36, 'Ốp lưng có chân đế Spigen Case Tough Armor cho Samsung Galaxy S10E', 'op-lung-co-chan-de-spigen-case-tough-armor-cho-samsung-galaxy-s10e', 250000, 0, 15, 0, 0, 9, 10, '2020-08-17__8.jpg', 1, 0, 1, 0, 12, 0, 'Ốp lưng có chân đế Spigen Case Tough Armor cho Samsung Galaxy S10E', 'Ốp lưng có chân đế Spigen Case Tough Armor cho Samsung Galaxy S10E', 0, 0, 0, '2020-08-17 04:18:03', '2020-08-17', 12, NULL, NULL, 0, NULL),
(37, 'Ốp lưng Spigen Ultra Hybrid S - iPhone 11 Pro', 'op-lung-spigen-ultra-hybrid-s-iphone-11-pro', 792000, 0, 15, 0, 0, 5, 10, '2020-08-17__9.jpg', 0, 0, 1, 0, 9, 0, 'Ốp lưng Spigen Ultra Hybrid S - iPhone 11 Pro', 'Ốp lưng Spigen Ultra Hybrid S - iPhone 11 Pro', 0, 0, 0, '2020-08-17 04:19:17', '2020-08-17', 9, NULL, NULL, 0, NULL),
(38, 'Ốp lưng cho iPhone XR - Spigen Case Ultra Hybrid', 'op-lung-cho-iphone-xr-spigen-case-ultra-hybrid', 350000, 0, 15, 0, 0, 6, 10, '2020-08-17__10.jpg', 3, 0, 1, 0, 9, 0, 'Ốp lưng cho iPhone XR - Spigen Case Ultra Hybrid', 'Ốp lưng cho iPhone XR - Spigen Case Ultra Hybrid', 0, 0, 0, '2020-08-17 04:19:50', '2020-08-17', 9, NULL, NULL, 0, NULL),
(39, 'Ốp lưng Spigen Case Slim Armor cho Samsung Galaxy S10E', 'op-lung-spigen-case-slim-armor-cho-samsung-galaxy-s10e', 360000, 0, 15, 0, 0, 0, 10, '2020-08-17__11.jpg', 1, 0, 1, 0, 4, 0, 'Ốp lưng Spigen Case Slim Armor cho Samsung Galaxy S10E', 'Ốp lưng Spigen Case Slim Armor cho Samsung Galaxy S10E', 0, 0, 0, '2020-08-17 04:20:43', '2020-08-17', 4, NULL, NULL, 0, NULL),
(40, 'Ốp lưng UAG chống sốc Monarch cho Samsung Galaxy S20 Plus', 'op-lung-uag-chong-soc-monarch-cho-samsung-galaxy-s20-plus', 330000, 0, 15, 0, 0, 4, 10, '2020-08-17__12.jpg', 0, 0, 1, 0, 9, 0, 'Ốp lưng UAG chống sốc Monarch cho Samsung Galaxy S20 Plus', 'Ốp lưng UAG chống sốc Monarch cho Samsung Galaxy S20 Plus', 0, 0, 0, '2020-08-17 04:21:22', '2020-08-17', 9, NULL, NULL, 0, NULL),
(41, 'Combo bàn phím + Chuột không dây Logitech MK240', 'combo-ban-phim-chuot-khong-day-logitech-mk240', 3500000, 0, 20, 0, 0, 8, 10, '2020-08-17__13.jpg', 0, 0, 1, 0, 8, 0, 'Combo bàn phím + Chuột không dây Logitech MK240', 'Combo bàn phím + Chuột không dây Logitech MK240', 0, 0, 0, '2020-08-17 04:22:30', '2020-08-17', 8, NULL, NULL, 0, NULL),
(42, 'Chuột không dây Logitech M331', 'chuot-khong-day-logitech-m331', 350000, 0, 20, 0, 0, 7, 10, '2020-08-17__1.jpg', 0, 0, 1, 0, 9, 0, 'Chuột không dây Logitech M331', 'Chuột không dây Logitech M331', 0, 0, 0, '2020-08-17 04:23:11', '2020-08-17', 9, NULL, NULL, 0, NULL),
(43, 'Bàn phím Bluetooth Titan KB02', 'ban-phim-bluetooth-titan-kb02', 3500000, 0, 20, 0, 0, 5, 10, '2020-08-17__2.jpg', 0, 0, 1, 0, 7, 0, 'Bàn phím Bluetooth Titan KB02', 'Bàn phím Bluetooth Titan KB02', 0, 0, 0, '2020-08-17 04:23:34', '2020-08-17', 7, NULL, NULL, 0, NULL),
(44, 'Chuột Gaming ASUS CERBERUS', 'chuot-gaming-asus-cerberus', 350000, 0, 20, 0, 0, 0, 10, '2020-08-17__3.webp', 0, 0, 1, 0, 7, 0, 'Chuột Gaming ASUS CERBERUS', 'Chuột Gaming ASUS CERBERUS', 0, 0, 0, '2020-08-17 04:23:57', '2020-08-17', 7, NULL, NULL, 0, NULL),
(45, 'Chuột không dây Logitech M590', 'chuot-khong-day-logitech-m590', 360000, 0, 20, 0, 0, 0, 10, '2020-08-17__4.jpg', 0, 0, 1, 0, 7, 0, 'Chuột không dây Logitech M590', 'Chuột không dây Logitech M590', 0, 0, 0, '2020-08-17 04:24:21', '2020-08-17', 7, NULL, NULL, 0, NULL),
(46, 'Bàn phím Apple Magic Keyboard 2', 'ban-phim-apple-magic-keyboard-2', 2500000, 0, 20, 0, 0, 7, 10, '2020-08-17__5.jpg', 0, 0, 1, 0, 9, 0, 'Bàn phím Apple Magic Keyboard 2', 'Bàn phím Apple Magic Keyboard 2', 0, 0, 0, '2020-08-17 04:24:47', '2020-08-17', 9, NULL, NULL, 0, NULL),
(47, 'Bàn phím Game có dây Logitech RGB G512', 'ban-phim-game-co-day-logitech-rgb-g512', 2500000, 0, 20, 0, 0, 1, 10, '2020-08-17__6.webp', 0, 0, 1, 0, 9, 0, 'Bàn phím Game có dây Logitech RGB G512', 'Bàn phím Game có dây Logitech RGB G512', 0, 0, 0, '2020-08-17 04:25:23', '2020-08-17', 9, NULL, NULL, 0, NULL),
(48, 'Bộ bàn phím chuột không dây Microsoft Wireless 850 ( PY9-00018 )', 'bo-ban-phim-chuot-khong-day-microsoft-wireless-850-py9-00018', 2390000, 0, 20, 0, 0, 10, 10, '2020-08-17__7.jpg', 0, 0, 1, 0, 9, 0, 'Bộ bàn phím chuột không dây Microsoft Wireless 850 ( PY9-00018 )', 'Bộ bàn phím chuột không dây Microsoft Wireless 850 ( PY9-00018 )', 0, 0, 0, '2020-08-17 04:25:55', '2020-08-17', 9, NULL, NULL, 0, NULL),
(49, 'Camera Xiaomi Mi Home Security 360 - 1080P', 'camera-xiaomi-mi-home-security-360-1080p', 3600000, 0, 19, 0, 0, 5, 10, '2020-08-17__8.jpg', 1, 0, 1, 0, 13, 0, 'Camera Xiaomi Mi Home Security 360 - 1080P', 'Camera Xiaomi Mi Home Security 360 - 1080P', 0, 0, 0, '2020-08-17 04:27:02', '2020-08-17', 13, NULL, NULL, 0, NULL),
(50, 'Camera hành trình GoPro Hero 8', 'camera-hanh-trinh-gopro-hero-8', 7900000, 0, 19, 0, 0, 5, 10, '2020-08-17__9.jpg', 1, 0, 1, 0, 8, 0, 'Camera hành trình GoPro Hero 8', 'Camera hành trình GoPro Hero 8', 0, 0, 0, '2020-08-17 04:27:27', '2020-08-17', 8, NULL, NULL, 0, NULL),
(51, 'Tay cầm chống rung Zhiyun Smooth 4', 'tay-cam-chong-rung-zhiyun-smooth-4', 1500000, 0, 19, 0, 0, 10, 10, '2020-08-17__10.jpg', 0, 0, 1, 0, 10, 0, 'Tay cầm chống rung Zhiyun Smooth 4', 'Tay cầm chống rung Zhiyun Smooth 4', 0, 0, 0, '2020-08-17 04:27:57', '2020-08-17', 10, NULL, NULL, 0, NULL),
(52, 'Camera IP Wifi Ezviz C3W 1080p', 'camera-ip-wifi-ezviz-c3w-1080p', 1300000, 0, 19, 0, 0, 10, 10, '2020-08-17__11.jpg', 0, 0, 1, 0, 6, 0, 'Camera IP Wifi Ezviz C3W 1080p', 'Camera IP Wifi Ezviz C3W 1080p', 0, 0, 0, '2020-08-17 04:28:30', '2020-08-17', 6, NULL, NULL, 0, NULL),
(53, 'Camera IP WIFI HIKVISON DS-2CD2421G0-IW 2MP 1080', 'camera-ip-wifi-hikvison-ds-2cd2421g0-iw-2mp-1080', 990000, 0, 19, 0, 0, 1, 10, '2020-08-17__12.jpg', 0, 0, 1, 0, 7, 0, 'Camera IP WIFI HIKVISON DS-2CD2421G0-IW 2MP 1080', 'Camera IP WIFI HIKVISON DS-2CD2421G0-IW 2MP 1080', 0, 0, 0, '2020-08-17 04:28:58', '2020-08-17', 7, NULL, NULL, 0, NULL),
(54, 'Camera IP Hồng ngoại không dây Ezviz 1MP 720p', 'camera-ip-hong-ngoai-khong-day-ezviz-1mp-720p', 360000, 0, 19, 0, 0, 1, 10, '2020-08-17__13.jpg', 1, 0, 1, 0, 6, 0, 'Camera IP Hồng ngoại không dây Ezviz 1MP 720p', 'Camera IP Hồng ngoại không dây Ezviz 1MP 720p', 0, 0, 0, '2020-08-17 04:29:31', '2020-08-17', 6, NULL, NULL, 0, NULL),
(55, 'Webcam A4tech 720p HD PK910P', 'webcam-a4tech-720p-hd-pk910p', 3500000, 0, 19, 0, 0, 0, 10, '2020-08-17__14.jpg', 0, 0, 1, 0, 2, 0, 'Webcam A4tech 720p HD PK910P', 'Webcam A4tech 720p HD PK910P', 0, 0, 0, '2020-08-17 04:29:58', '2020-08-17', 2, NULL, NULL, 0, NULL),
(56, 'Giá đỡ điện thoại thời trang Popsockets', 'gia-do-dien-thoai-thoi-trang-popsockets', 30000, 0, 29, 0, 0, 1, 10, '2020-08-17__1.jpg', 0, 0, 1, 0, 6, 0, 'Giá đỡ điện thoại thời trang Popsockets', 'Giá đỡ điện thoại thời trang Popsockets', 0, 0, 0, '2020-08-17 07:43:46', '2020-08-17', 6, NULL, NULL, 0, NULL),
(57, 'Giá đỡ Laptop/Macbook Nhôm MS01', 'gia-do-laptopmacbook-nhom-ms01', 350000, 0, 29, 0, 0, 0, 10, '2020-08-17__2.webp', 0, 0, 1, 0, 3, 0, 'Giá đỡ Laptop/Macbook Nhôm MS01', 'Giá đỡ Laptop/Macbook Nhôm MS01', 0, 0, 0, '2020-08-17 07:44:15', '2020-08-17', 3, NULL, NULL, 0, NULL),
(58, 'Giá đỡ điện thoại/máy tính bảng Nillkin hộp kim nhôm cao cấp', 'gia-do-dien-thoaimay-tinh-bang-nillkin-hop-kim-nhom-cao-cap', 350000, 0, 29, 0, 0, 0, 10, '2020-08-17__3.webp', 0, 0, 1, 0, 5, 0, 'Giá đỡ điện thoại/máy tính bảng Nillkin hộp kim nhôm cao cấp', 'Giá đỡ điện thoại/máy tính bảng Nillkin hộp kim nhôm cao cấp', 0, 0, 0, '2020-08-17 08:13:59', '2020-08-17', 5, NULL, NULL, 0, NULL),
(59, 'Giá đỡ điện thoại trên xe hơi Pisen QV-639', 'gia-do-dien-thoai-tren-xe-hoi-pisen-qv-639', 3600000, 0, 29, 0, 0, 10, 10, '2020-08-17__4.webp', 0, 0, 1, 0, 7, 0, 'Giá đỡ điện thoại trên xe hơi Pisen QV-639', 'Giá đỡ điện thoại trên xe hơi Pisen QV-639', 0, 0, 0, '2020-08-17 08:14:29', '2020-08-17', 7, NULL, NULL, 0, NULL),
(60, 'Giá đỡ điện thoại/máy tính bảng S-Case hợp kim nhôm', 'gia-do-dien-thoaimay-tinh-bang-s-case-hop-kim-nhom', 350000, 0, 29, 0, 0, 0, 10, '2020-08-17__5.jpg', 0, 0, 1, 0, 3, 0, 'Giá đỡ điện thoại/máy tính bảng S-Case hợp kim nhôm', 'Giá đỡ điện thoại/máy tính bảng S-Case hợp kim nhôm', 0, 0, 0, '2020-08-17 08:15:24', '2020-08-17', 3, NULL, NULL, 0, NULL),
(61, 'Giá đỡ Laptop/Macbook S-Case High Stand Nhôm nguyên khối MS03', 'gia-do-laptopmacbook-s-case-high-stand-nhom-nguyen-khoi-ms03', 850000, 0, 29, 0, 0, 6, 10, '2020-08-17__6.webp', 0, 0, 1, 0, 9, 0, 'Giá đỡ Laptop/Macbook S-Case High Stand Nhôm nguyên khối MS03', 'Giá đỡ Laptop/Macbook S-Case High Stand Nhôm nguyên khối MS03', 0, 0, 0, '2020-08-17 08:15:49', '2020-08-17', 9, NULL, NULL, 0, NULL),
(62, 'Giá đỡ Laptop/Macbook hộp kim nhôm đa năng cao cấp', 'gia-do-laptopmacbook-hop-kim-nhom-da-nang-cao-cap', 450000, 0, 29, 0, 0, 0, 10, '2020-08-17__7.jpg', 0, 0, 1, 0, 5, 0, 'Giá đỡ Laptop/Macbook hộp kim nhôm đa năng cao cấp', 'Giá đỡ Laptop/Macbook hộp kim nhôm đa năng cao cấp', 0, 0, 0, '2020-08-17 08:17:02', '2020-08-17', 5, NULL, NULL, 0, NULL),
(63, 'Giá đỡ điện thoại/máy tính bảng Baseus hộp kim nhôm cao cấp', 'gia-do-dien-thoaimay-tinh-bang-baseus-hop-kim-nhom-cao-cap', 750000, 0, 29, 0, 0, 5, 10, '2020-08-17__8.webp', 0, 0, 1, 0, 8, 0, 'Giá đỡ điện thoại/máy tính bảng Baseus hộp kim nhôm cao cấp', 'Giá đỡ điện thoại/máy tính bảng Baseus hộp kim nhôm cao cấp', 0, 0, 0, '2020-08-17 08:17:26', '2020-08-17', 8, NULL, NULL, 0, NULL),
(64, 'Giá đỡ điện thoại/máy tính bảng Baseus unlimited holder', 'gia-do-dien-thoaimay-tinh-bang-baseus-unlimited-holder', 2300000, 0, 29, 0, 0, 0, 10, '2020-08-17__9.webp', 0, 0, 1, 0, 6, 0, 'Giá đỡ điện thoại/máy tính bảng Baseus unlimited holder', 'Giá đỡ điện thoại/máy tính bảng Baseus unlimited holder', 0, 0, 0, '2020-08-17 08:18:04', '2020-08-17', 6, NULL, NULL, 0, NULL),
(65, 'Giá đỡ điện thoại / máy tính bảng S-case kim loại', 'gia-do-dien-thoai-may-tinh-bang-s-case-kim-loai', 100000, 0, 29, 0, 0, 5, 10, '2020-08-17__10.webp', 0, 0, 1, 0, 8, 0, 'Giá đỡ điện thoại / máy tính bảng S-case kim loại', 'Giá đỡ điện thoại / máy tính bảng S-case kim loại', 0, 0, 0, '2020-08-17 08:18:31', '2020-08-17', 8, NULL, NULL, 0, NULL),
(66, 'Giá đỡ Laptop/Macbook S-Case Jobson loại gập MS04', 'gia-do-laptopmacbook-s-case-jobson-loai-gap-ms04', 230000, 0, 29, 0, 0, 0, 10, '2020-08-17__11.webp', 0, 0, 1, 0, 8, 0, 'Giá đỡ Laptop/Macbook S-Case Jobson loại gập MS04', 'Giá đỡ Laptop/Macbook S-Case Jobson loại gập MS04', 0, 0, 0, '2020-08-17 08:19:04', '2020-08-17', 8, NULL, NULL, 0, NULL),
(67, 'Quạt mini để bàn Pisen telescopic folding TP-F05GXZ', 'quat-mini-de-ban-pisen-telescopic-folding-tp-f05gxz', 850000, 0, 30, 0, 0, 0, 10, '2020-08-17__12.jpg', 0, 0, 1, 0, 6, 0, 'Quạt mini để bàn Pisen telescopic folding TP-F05GXZ', 'Quạt mini để bàn Pisen telescopic folding TP-F05GXZ', 0, 0, 0, '2020-08-17 08:19:45', '2020-08-17', 6, NULL, NULL, 0, NULL),
(68, 'Quạt Cầm Tay Mini', 'quat-cam-tay-mini', 100000, 0, 30, 0, 0, 0, 10, '2020-08-17__1.webp', 0, 0, 1, 0, 5, 0, 'Quạt Cầm Tay Mini', 'Quạt Cầm Tay Mini', 0, 0, 0, '2020-08-17 08:20:26', '2020-08-17', 5, NULL, NULL, 0, NULL),
(69, 'Quạt mini cầm tay Baseus Firefly', 'quat-mini-cam-tay-baseus-firefly', 250000, 0, 30, 0, 0, 7, 10, '2020-08-17__2.jpg', 0, 0, 1, 0, 11, 0, 'Quạt mini cầm tay Baseus Firefly', 'Quạt mini cầm tay Baseus Firefly', 0, 0, 0, '2020-08-17 08:20:58', '2020-08-17', 11, NULL, NULL, 0, NULL),
(70, 'Quạt mini cầm tay Benks Handhelp', 'quat-mini-cam-tay-benks-handhelp', 225000, 0, 30, 0, 0, 0, 10, '2020-08-17__3.jpg', 0, 0, 1, 0, 8, 0, 'Quạt mini cầm tay Benks Handhelp', 'Quạt mini cầm tay Benks Handhelp', 0, 0, 0, '2020-08-17 08:21:34', '2020-08-17', 8, NULL, NULL, 0, NULL),
(71, 'Quạt đế kẹp Titan QT03', 'quat-de-kep-titan-qt03', 350000, 0, 30, 0, 0, 0, 10, '2020-08-17__4.jpg', 0, 0, 1, 0, 7, 0, 'Quạt đế kẹp Titan QT03', 'Quạt đế kẹp Titan QT', 0, 0, 0, '2020-08-17 08:22:11', '2020-08-17', 7, NULL, NULL, 0, NULL),
(72, 'Quạt mini để bàn Baseus cube saking fan', 'quat-mini-de-ban-baseus-cube-saking-fan', 250000, 0, 30, 0, 0, 0, 10, '2020-08-17__5.webp', 0, 0, 1, 0, 5, 0, 'Quạt mini để bàn Baseus cube saking fan', 'Quạt mini để bàn Baseus cube saking fan', 0, 0, 0, '2020-08-17 08:22:35', '2020-08-17', 5, NULL, NULL, 0, NULL),
(73, 'Quạt mini để bàn Baseus Ocean', 'quat-mini-de-ban-baseus-ocean', 350000, 0, 30, 0, 0, 0, 10, '2020-08-17__6.jpg', 0, 0, 1, 0, 5, 0, 'Quạt mini để bàn Baseus Ocean', 'Quạt mini để bàn Baseus Ocean', 0, 0, 0, '2020-08-17 08:22:55', '2020-08-17', 5, NULL, NULL, 0, NULL),
(74, 'Quạt Mini cầm tay Pisen Meatball Có Gương', 'quat-mini-cam-tay-pisen-meatball-co-guong', 250000, 0, 30, 0, 0, 0, 10, '2020-08-17__7.jpg', 0, 0, 1, 0, 5, 0, 'Quạt Mini cầm tay Pisen Meatball Có Gương', 'Quạt Mini cầm tay Pisen Meatball Có Gương', 0, 0, 0, '2020-08-17 08:23:14', '2020-08-17', 5, NULL, NULL, 0, NULL),
(75, 'Quạt mini cầm tay Pisen meatball handheld F828', 'quat-mini-cam-tay-pisen-meatball-handheld-f828', 270000, 0, 30, 0, 0, 0, 10, '2020-08-17__8.jpg', 0, 0, 1, 0, 4, 0, 'Quạt mini cầm tay Pisen meatball handheld F828', 'Quạt mini cầm tay Pisen meatball handheld F828', 0, 0, 0, '2020-08-17 08:23:35', '2020-08-17', 4, NULL, NULL, 0, NULL),
(76, 'Quạt mini để bàn tích điện gấp gọn SwitchEasy SwitchFan', 'quat-mini-de-ban-tich-dien-gap-gon-switcheasy-switchfan', 1200000, 0, 30, 0, 0, 5, 10, '2020-08-17__9.jpg', 0, 0, 1, 0, 7, 0, 'Quạt mini để bàn tích điện gấp gọn SwitchEasy SwitchFan', 'Quạt mini để bàn tích điện gấp gọn SwitchEasy SwitchFan', 0, 0, 0, '2020-08-17 08:24:10', '2020-08-17', 7, NULL, NULL, 0, NULL),
(77, 'Quạt mini đế kẹp Baseus tích điện xoay góc 360 độ', 'quat-mini-de-kep-baseus-tich-dien-xoay-goc-360-do', 250000, 0, 30, 0, 0, 0, 10, '2020-08-17__10.jpg', 9, 0, 1, 2, 5, 0, 'Quạt mini đế kẹp Baseus tích điện xoay góc 360 độ', 'Quạt mini đế kẹp Baseus tích điện xoay góc 360 độ', 1, 5, 5, '2020-08-17 08:24:48', '2020-08-17', 5, NULL, NULL, 0, '2020-10-10 08:46:12'),
(78, 'Quạt mini cầm tay Pisen Oscillating TP-F04GXZ', 'quat-mini-cam-tay-pisen-oscillating-tp-f04gxz', 1190000, 0, 30, 0, 0, 5, 10, '2020-08-17__11.jpg', 1, 0, 1, 0, 10, 0, 'Quạt mini cầm tay Pisen Oscillating TP-F04GXZ', 'Quạt mini cầm tay Pisen Oscillating TP-F04GXZ', 0, 0, 0, '2020-08-17 08:25:13', '2020-08-17', 10, NULL, NULL, 0, NULL),
(79, 'Bàn phím Apple Smart Keyboard cho iPad Pro 12.9 2020', 'ban-phim-apple-smart-keyboard-cho-ipad-pro-129-2020', 0, 0, 20, 0, 0, 0, 10, '2020-08-17__13.jpg', 0, 0, 1, 0, 0, 0, 'Bàn phím Apple Smart Keyboard cho iPad Pro 12.9 2020', 'Bàn phím Apple Smart Keyboard cho iPad Pro 12.9 2020', 0, 0, 0, '2020-08-17 08:45:46', NULL, 0, NULL, NULL, 0, NULL),
(80, 'Sạc Macbook Apple 30W USB-C Power Adapter Chính hãng', 'sac-macbook-apple-30w-usb-c-power-adapter-chinh-hang', 1400000, 0, 16, 0, 0, 0, 10, '2020-08-17__14.webp', 0, 0, 1, 0, 6, 0, 'Sạc Macbook Apple 30W USB-C Power Adapter Chính hãng', 'Sạc Macbook Apple 30W USB-C Power Adapter Chính hãng', 0, 0, 0, '2020-08-17 08:46:18', '2020-08-17', 6, NULL, NULL, 0, NULL),
(81, 'Sạc Macbook Apple 87W USB-C Power Adapter MNF82 Chính hãng', 'sac-macbook-apple-87w-usb-c-power-adapter-mnf82-chinh-hang', 2500000, 0, 16, 0, 0, 0, 10, '2020-08-17__15.webp', 0, 0, 1, 0, 4, 0, 'Sạc Macbook Apple 87W USB-C Power Adapter MNF82 Chính hãng', 'Sạc Macbook Apple 87W USB-C Power Adapter MNF82 Chính hãng', 0, 0, 0, '2020-08-17 08:46:44', '2020-08-17', 4, NULL, NULL, 0, NULL),
(82, 'Bút cảm ứng Apple Pencil 2 Đã kích hoạt bảo hành', 'but-cam-ung-apple-pencil-2-da-kich-hoat-bao-hanh', 3500000, 0, 28, 0, 0, 4, 10, '2020-08-17__16.webp', 0, 0, 1, 0, 6, 0, 'Bút cảm ứng Apple Pencil 2 Đã kích hoạt bảo hành', 'Bút cảm ứng Apple Pencil 2 Đã kích hoạt bảo hành', 0, 0, 0, '2020-08-17 08:47:11', '2020-08-17', 6, NULL, NULL, 0, NULL),
(83, 'Pin dự phòng Energizer 15000 mAh QC 3.0 UE15002CQ', 'pin-du-phong-energizer-15000-mah-qc-30-ue15002cq', 850000, 0, 17, 0, 0, 5, 10, '2020-08-17__17.jpg', 6, 0, 1, 1, 11, 0, 'Pin dự phòng Energizer 15000 mAh QC 3.0 UE15002CQ', 'Pin dự phòng Energizer 15000 mAh QC 3.0 UE15002CQ', 0, 0, 0, '2020-08-17 08:47:57', '2020-08-17', 11, NULL, NULL, 0, NULL),
(84, 'Pin sạc dự phòng Energizer 10000mAh - UE10028PQ', 'pin-sac-du-phong-energizer-10000mah-ue10028pq', 690000, 0, 17, 0, 0, 7, 10, '2020-08-17__18.webp', 2, 0, 1, 0, 7, 0, 'Pin sạc dự phòng Energizer 10000mAh - UE10028PQ', 'Pin sạc dự phòng Energizer 10000mAh - UE10028PQ', 0, 0, 0, '2020-08-17 08:48:22', '2020-08-17', 7, NULL, NULL, 0, NULL),
(85, 'Tai nghe bluetooth Mèo', 'tai-nghe-bluetooth-meo', 1250, 0, 28, 0, 0, 5, 10, '2020-11-10__ff50a39b759ecd2c7e1395b79c1fa17d.png', 1, 0, 1, 0, 2, 0, 'Tai nghe bluetooth Mèo', 'Tai nghe bluetooth Mèo', 0, 0, 0, '2020-11-10 11:11:00', '2020-11-10', 2, NULL, NULL, 0, '2020-11-10 11:17:01'),
(86, 'Loa', 'loa', 1450, 0, 28, 0, 0, 2, 10, NULL, 0, 0, 1, 0, 2, 0, 'Loa', 'Loa', 0, 0, 0, '2020-11-12 06:36:36', '2020-11-12', 2, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pa_product_id` int(11) NOT NULL DEFAULT '0',
  `pa_attribute_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_attributes`
--

INSERT INTO `products_attributes` (`id`, `pa_product_id`, `pa_attribute_id`) VALUES
(2, 85, 1),
(3, 86, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_keywords`
--

CREATE TABLE `products_keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pk_product_id` int(11) NOT NULL DEFAULT '0',
  `pk_keyword_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pi_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pi_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pi_product_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_invoice_entered`
--

CREATE TABLE `product_invoice_entered` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pie_product_id` int(11) NOT NULL DEFAULT '0',
  `pie_invoice_entered_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `r_user_id` int(11) NOT NULL DEFAULT '0',
  `r_product_id` int(11) NOT NULL DEFAULT '0',
  `r_number` tinyint(4) NOT NULL DEFAULT '0',
  `r_status` tinyint(4) NOT NULL DEFAULT '0',
  `r_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `r_user_id`, `r_product_id`, `r_number`, `r_status`, `r_content`, `created_at`, `updated_at`) VALUES
(1, 2, 77, 5, 0, 'tốt', '2020-10-10 08:46:12', '2020-10-10 08:46:12');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `name_slug`, `guard_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Superadmin', 'superadmin', 'admins', NULL, '2020-10-10 09:29:17', '2020-10-10 09:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sd_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sd_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sd_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sd_target` tinyint(4) NOT NULL DEFAULT '1',
  `sd_active` tinyint(4) NOT NULL DEFAULT '1',
  `sd_sort` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `sd_title`, `sd_link`, `sd_image`, `sd_target`, `sd_active`, `sd_sort`, `created_at`, `updated_at`) VALUES
(1, 'Slide 1', 'abc.com', '2020-10-10__hh-8.png', 2, 1, 1, '2020-10-10 09:03:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statics`
--

CREATE TABLE `statics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `s_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_type` tinyint(4) NOT NULL DEFAULT '0',
  `s_md5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statics`
--

INSERT INTO `statics` (`id`, `s_title`, `s_slug`, `s_type`, `s_md5`, `s_content`, `created_at`, `updated_at`) VALUES
(1, 'Hướng dẫn mua hàng', NULL, 1, NULL, '<p>Hướng dẫn mua h&agrave;ng</p>', '2020-10-10 09:05:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplieres`
--

CREATE TABLE `supplieres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sl_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sl_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sl_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sl_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplieres`
--

INSERT INTO `supplieres` (`id`, `sl_name`, `sl_phone`, `sl_email`, `sl_address`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Minh Hằng', '0123456789', 'hang@gmail.com', 'so 1', '2020-10-10 08:40:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tst_user_id` int(11) NOT NULL DEFAULT '0',
  `tst_admin_id` int(11) NOT NULL DEFAULT '0',
  `tst_total_money` int(11) NOT NULL DEFAULT '0',
  `tst_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tst_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tst_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tst_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tst_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tst_status` tinyint(4) NOT NULL DEFAULT '1',
  `tst_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT ' 1 thanh toan thuong, 2 la thanh toan online',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `tst_user_id`, `tst_admin_id`, `tst_total_money`, `tst_name`, `tst_email`, `tst_phone`, `tst_address`, `tst_note`, `tst_status`, `tst_type`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 500000, 'Khách Hàng', 'doantotnghiep@gmail.com', '0986420994', 'Nghe An', NULL, 1, 1, '2020-08-17 10:06:59', NULL),
(2, 2, 0, 807500, 'Nguyen Van C', 'nguyenvanc@gmail.com', '0123456789', '32', NULL, -1, 1, '2020-10-10 08:27:43', '2020-10-10 08:53:17'),
(3, 1, 0, 250000, 'Khách Hàng', 'doantotnghiep@gmail.com', '0986420994', 'Nghe NA', NULL, 1, 1, '2020-10-28 09:22:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `log_login` text COLLATE utf8mb4_unicode_ci,
  `count_comment` tinyint(4) NOT NULL DEFAULT '0',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `balance`, `log_login`, `count_comment`, `address`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Khách Hàng', 'doantotnghiep@gmail.com', NULL, '$2y$10$nph0PAwtzOSGFOhMR3rzc.pxRJHPYZTXVo1wNc5C6gUZNsNm4DEVG', '0986420994', 0, '[{\"device\":\"WebKit\",\"platform\":\"Windows\",\"platform_ver\":\"10.0\",\"browser\":\"Chrome\",\"browser_ver\":\"85.0.4183.121\",\"time\":\"2020-10-05T01:29:48.873076Z\"},{\"device\":\"Macintosh\",\"platform\":\"OS X\",\"platform_ver\":\"11_0_0\",\"browser\":\"Chrome\",\"browser_ver\":\"86.0.4240.111\",\"time\":\"2020-10-27T05:29:25.693141Z\"},{\"device\":\"Macintosh\",\"platform\":\"OS X\",\"platform_ver\":\"10_15_6\",\"browser\":\"Chrome\",\"browser_ver\":\"86.0.4240.111\",\"time\":\"2020-10-28T09:22:09.381254Z\"},{\"device\":\"WebKit\",\"platform\":\"Windows\",\"platform_ver\":\"10.0\",\"browser\":\"Chrome\",\"browser_ver\":\"86.0.4240.111\",\"time\":\"2020-11-05T01:34:58.603111Z\"},{\"device\":\"WebKit\",\"platform\":\"Windows\",\"platform_ver\":\"10.0\",\"browser\":\"Chrome\",\"browser_ver\":\"86.0.4240.111\",\"time\":\"2020-11-05T04:54:38.633500Z\"}]', 0, NULL, NULL, NULL, '2020-08-17 10:05:04', NULL),
(2, 'Nguyen Van C', 'nguyenvanc@gmail.com', NULL, '$2y$10$imlyAUyB4ZFUk9I3Lil20OJw.Gn52ekzFParPlbTO6D0Dmn/ncyKK', '0123456789', 0, NULL, 0, NULL, NULL, NULL, '2020-10-10 08:27:33', NULL),
(3, 'Dang Van Duy', 'duyd433@gmail.com', NULL, '$2y$10$IeMBLzwqCl8q0fy6j.Hsauw8T/959kBQCOFsqm2ApP6gEzCF5gD5K', '0972120310', 0, NULL, 0, NULL, NULL, NULL, '2020-10-27 07:20:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_favourite`
--

CREATE TABLE `user_favourite` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uf_product_id` int(11) NOT NULL DEFAULT '0',
  `uf_user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_phone_unique` (`phone`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_a_slug_index` (`a_slug`),
  ADD KEY `articles_a_hot_index` (`a_hot`),
  ADD KEY `articles_a_active_index` (`a_active`),
  ADD KEY `articles_a_menu_id_index` (`a_menu_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_atb_name_unique` (`atb_name`),
  ADD KEY `attributes_atb_category_id_index` (`atb_category_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_c_slug_unique` (`c_slug`),
  ADD KEY `categories_c_parent_id_index` (`c_parent_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_cmt_parent_id_index` (`cmt_parent_id`),
  ADD KEY `comments_cmt_product_id_index` (`cmt_product_id`),
  ADD KEY `comments_cmt_admin_id_index` (`cmt_admin_id`),
  ADD KEY `comments_cmt_user_id_index` (`cmt_user_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_entered`
--
ALTER TABLE `invoice_entered`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_entered_ie_suppliere_index` (`ie_suppliere`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `keywords_k_slug_unique` (`k_slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_mn_slug_unique` (`mn_slug`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pay_histories`
--
ALTER TABLE `pay_histories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pay_histories_ph_code_unique` (`ph_code`),
  ADD KEY `index_code_user_id` (`ph_code`,`ph_user_id`),
  ADD KEY `pay_histories_ph_user_id_index` (`ph_user_id`);

--
-- Indexes for table `pay_ins`
--
ALTER TABLE `pay_ins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_ins_pi_user_id_index` (`pi_user_id`),
  ADD KEY `pay_ins_pi_admin_id_index` (`pi_admin_id`);

--
-- Indexes for table `pay_outs`
--
ALTER TABLE `pay_outs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_outs_po_user_id_index` (`po_user_id`),
  ADD KEY `pay_outs_po_transaction_id_index` (`po_transaction_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_pro_slug_unique` (`pro_slug`),
  ADD KEY `products_pro_hot_index` (`pro_hot`),
  ADD KEY `products_pro_active_index` (`pro_active`),
  ADD KEY `products_pro_supplier_id_index` (`pro_supplier_id`);

--
-- Indexes for table `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_attributes_pa_product_id_index` (`pa_product_id`),
  ADD KEY `products_attributes_pa_attribute_id_index` (`pa_attribute_id`);

--
-- Indexes for table `products_keywords`
--
ALTER TABLE `products_keywords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_keywords_pk_product_id_index` (`pk_product_id`),
  ADD KEY `products_keywords_pk_keyword_id_index` (`pk_keyword_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_invoice_entered`
--
ALTER TABLE `product_invoice_entered`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_invoice_entered_pie_product_id_index` (`pie_product_id`),
  ADD KEY `product_invoice_entered_pie_invoice_entered_id_index` (`pie_invoice_entered_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_slug_unique` (`name_slug`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_accounts_provider_user_id_provider_unique` (`provider_user_id`,`provider`);

--
-- Indexes for table `statics`
--
ALTER TABLE `statics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplieres`
--
ALTER TABLE `supplieres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_tst_user_id_index` (`tst_user_id`),
  ADD KEY `transactions_tst_admin_id_index` (`tst_admin_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD KEY `users_balance_index` (`balance`);

--
-- Indexes for table `user_favourite`
--
ALTER TABLE `user_favourite`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_favourite_uf_product_id_uf_user_id_unique` (`uf_product_id`,`uf_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_entered`
--
ALTER TABLE `invoice_entered`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pay_histories`
--
ALTER TABLE `pay_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pay_ins`
--
ALTER TABLE `pay_ins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pay_outs`
--
ALTER TABLE `pay_outs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products_keywords`
--
ALTER TABLE `products_keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_invoice_entered`
--
ALTER TABLE `product_invoice_entered`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statics`
--
ALTER TABLE `statics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplieres`
--
ALTER TABLE `supplieres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_favourite`
--
ALTER TABLE `user_favourite`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
